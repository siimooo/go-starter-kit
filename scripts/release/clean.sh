#!/usr/bin/env bash
docker stop app1
docker rm app1
docker rmi app1-docker-optimized
docker rmi go-builder
docker rmi alpine
docker rmi golang