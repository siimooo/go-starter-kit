package archive
//
//import (
//	"bitbucket.com/siimooo/go-starter-kit/internal/pkg/util"
//	"database/sql"
//	"errors"
//	_ "github.com/go-sql-driver/mysql"
//	"strconv"
//)
//
////const (
////	// INSERT
////	InsertItemSql			= "INSERT INTO item (hyperlink, name, description, piece, unit) VALUES (:hyperlink, :name, :description, :piece, :unit)"
////	InsertCategorySql		= "INSERT INTO category (name, parent_id, description, created_at, order_at) VALUES (:name, :parent_id, :description, :created_at, :order_at)"
////	InsertSectionSql 		= "INSERT INTO section (item_id, category_id, order_at) VALUE (:item_id, :category_id, :order_at)"
////	// SELECT SECTION
////	SelectSectionItemsByCategoryId 	= "SELECT id, item_id, order_at FROM section WHERE category_id = ?"
////
////	// SELECT ITEM
////	SelectItems 			= "SELECT id, hyperlink, name, description, piece, unit FROM item"
////	SelectItemById 			= "SELECT id, hyperlink, name, description, piece, unit FROM item WHERE id = ?"
////	// SELECT CATEGORY
////	SelectSectionNodes = "SELECT name, id, description, order_at, parent_id FROM category WHERE parent_id = ? ORDER BY order_at"
////	SelectNode         = "SELECT id, name, description, created_at, order_at FROM category WHERE id = ?"
////	SelectNodes        = "SELECT id, name, description, created_at, order_at FROM category WHERE parent_id = ?"
////	SelectLeafNodes    = "SELECT c1.id, c1.name, c1.description, c1.created_at, c1.order_at FROM category c1 LEFT JOIN category c2 ON c2.parent_id = c1.id WHERE c2.id IS NULL"
////	SelectTree 				= "WITH RECURSIVE category_path (id, name, path) AS(SELECT id, name, name as path FROM category WHERE parent_id IS NULL UNION ALL SELECT c.id, c.name, CONCAT(cp.path, ' > ', c.name) FROM category_path AS cp JOIN category AS c ON cp.id = c.parent_id) SELECT * FROM category_path ORDER BY path"
////	SelectTreeByLvl 		= "WITH RECURSIVE category_path (id, name, level, description, created_at, order_at, parent_id) AS (SELECT id, name, 0 level, description, created_at, order_at, parent_id FROM category WHERE parent_id IS NULL UNION ALL SELECT c.id, c.name, cp.level + 1, c.description, c.created_at, c.order_at, c.parent_id FROM category_path AS cp JOIN category AS c ON cp.id = c.parent_id) SELECT * FROM category_path where level = ? ORDER BY level"
////
////	// DELETE
////	DeleteSectionSql 		= "DELETE FROM section"
////	DeleteListItemSql 		= "DELETE FROM item"
////	DeleteCategorySql 		= "DELETE FROM category"
////)
//
//
//
//
//
////Errors
//var (
//	//ErrShoppingListNotFound = errors.New("shopping_list: did not found any shopping list from database")
//	//ErrShoppingListItemNotFound = errors.New("shopping_list: did not found any shopping list item from database")
//	//ErrRootNodeNotExist = errors.New("shopping_list: root node not exist")
//	//ErrRootNodeExist = errors.New("shopping_list: root node already exist")
//	ErrNodeNotExist = errors.New("shopping_list: node not exist")
//)
//
//type ShoppingListRepository interface {
//	DeleteItems()
//	DeleteCategories()
//	DeleteSections()
//
//	InsertSectionItem(categoryId int64, itemId int64, orderAt int) error
//	SelectSectionItemsByParentId(categoryId int64) ([]SectionItem, error)
//
//	InsertItem(item Item) (int64, error)
//	InsertCategory(category Category) (int64, error)
//
//	//This is the primary node, it is usually the node that does not have a parent but is a parent for one or more entities.
//	InsertRootNode(name string, description string) (int64, error)
//	//These are the node that has one or more children.
//	InsertNode(parentId int64, name string, orderAt int32) (int64, error)
//	InsertNodes(parentId int64, names []string) ([]int64, error)
//	InsertNewSectionNode(parentId int64) (int64, error)
//	InsertSectionNode(rootId int64, name string, orderAt int32) (int64, error)
//
//	SelectItems() ([]Item, error)//TODO: this
//	SelectItemById(itemId int64) (*Item, error)
//
//	SelectSectionNodes(parentId int64) ([]SectionNode, error)
//	//SelectRootNode() (*Node, error)
//	//These are the node that has one or more children.
//	SelectNodes(parentId int64) ([]Node, error)
//	SelectNode(parentId int64) (*Node, error)
//
//	//The leaf nodes are the nodes that have no children.
//	SelectLeafNodes() ([]Node, error)
//	SelectTree() ([]PathNode, error)
//	SelectNodesByLevel(level int) ([]NodeLevel, error)
//}
//
//
//func NewMySQLShoppingList() *mysqlDBShoppingListRepository {
//	return &mysqlDBShoppingListRepository{db: nil}
//}
//
//type mysqlDBShoppingListRepository struct {
//	db *MySQLDatabase
//}
//
//func (mysql *mysqlDBShoppingListRepository) SelectSectionItemsByParentId(categoryId int64) ([]SectionItem, error) {
//	var sectionItems []SectionItem
//	if err := mysql.db.sqlx.Select(&sectionItems, SelectSectionItemsByCategoryId, categoryId); err != nil {
//		return sectionItems, err
//	}
//	return sectionItems, nil
//}
//
//func (mysql *mysqlDBShoppingListRepository) InsertNode(parentId int64, name string, orderAt int32) (int64, error) {
//	if id, err := mysql.InsertCategory(Category{Description: sql.NullString{"TODO: " + name + "-write-description..", true}, Name:name, ParentId:sql.NullInt64{Int64:int64(parentId), Valid:true}, OrderAt: orderAt}); err != nil {
//		return -1, err
//	} else {
//		return id, nil
//	}
//}
//
//func (mysql *mysqlDBShoppingListRepository) InsertSectionNode(rootId int64, name string, orderAt int32) (int64, error){
//	nodeId, err := mysql.InsertNode(rootId, name,orderAt)
//	if err != nil {
//		return -1, err
//	}
//	sectionId, err := mysql.InsertNewSectionNode(nodeId)
//	if err != nil {
//		return -1, err
//	}
//	return sectionId, nil
//}
//
//func (mysql *mysqlDBShoppingListRepository) DeleteSections() {
//	mysql.db.sqlx.MustExec(DeleteSectionSql)
//}
//
//func (mysql *mysqlDBShoppingListRepository) InsertSectionItem(categoryId int64, itemId int64, orderAt int) error {
//	_, err := mysql.db.sqlx.NamedExec(InsertSectionSql,map[string]interface{}{"item_id": itemId, "category_id": categoryId, "order_at": orderAt})
//	if err != nil {
//		return err
//	}
//	return nil
//}
//
//func (mysql *mysqlDBShoppingListRepository) SelectItemById(itemId int64) (*Item, error) {
//	item := Item{}
//	if err := mysql.db.sqlx.Get(&item, SelectItemById, itemId); err != nil {
//		return nil, err
//	}
//	return &item, nil
//}
//
//func (mysql *mysqlDBShoppingListRepository) SelectItems() ([]Item, error) {
//	var items []Item
//	if err := mysql.db.sqlx.Select(&items, SelectItems); err != nil {
//		return items, err
//	}
//	return items, nil
//}
//
//func (mysql *mysqlDBShoppingListRepository) SelectTree() ([]PathNode, error) {
//	//var pathNodes []PathNode
//	//if err := mysql.db.sqlx.Select(&pathNodes, SelectTree); err != nil {
//	//	return pathNodes, err
//	//}
//	//return pathNodes, nil
//	panic("a")
//}
//
//func (mysql *mysqlDBShoppingListRepository) SelectNodesByLevel(level int) ([]NodeLevel, error) {
//	//var nodes []NodeLevel
//	//if err := mysql.db.sqlx.Select(&nodes, SelectTreeByLvl, level); err != nil {
//	//	return nodes, err
//	//}
//	//return nodes, nil
//	panic("a")
//}
//
//func (mysql *mysqlDBShoppingListRepository) SelectSectionNodes(parentId int64) ([]SectionNode, error) {
//	//var nodes []SectionNode
//	//if err := mysql.db.sqlx.Select(&nodes, SelectSectionNodes, parentId); err != nil {
//	//	return nodes, err
//	//}
//	//return nodes, nil
//	panic("a")
//}
//
//func (mysql *mysqlDBShoppingListRepository) SelectNode(parentId int64) (*Node, error) {
//	//node := Node{}
//	//if err := mysql.db.sqlx.Get(&node, SelectNode, parentId); err != nil {
//	//	if err == sql.ErrNoRows {
//	//		return nil, ErrNodeNotExist
//	//	}
//	//	return nil, err
//	//}
//	//return &node, nil
//	panic("a")
//}
//
//func (mysql *mysqlDBShoppingListRepository) InsertNewSectionNode(parentId int64) (int64, error) {
//	nodes, err := mysql.SelectSectionNodes(parentId)
//	if err != nil {
//		return -1, err
//	}
//	node, err := mysql.SelectNode(parentId)
//	if err != nil {
//		return -1, err
//	}
//	nodesLen := len(nodes)
//	section := node.Name+"-section"+strconv.Itoa(nodesLen)
//	sectionId, err := mysql.InsertCategory(Category{ParentId:sql.NullInt64{Int64:int64(parentId), Valid:true}, Name: section, OrderAt:int32(nodesLen)})
//	if err != nil {
//		return -1, err
//	}
//	return sectionId, nil
//}
//
//
//func (mysql *mysqlDBShoppingListRepository) SelectLeafNodes() ([]Node, error) {
//	//var nodes []Node
//	//if err := mysql.db.sqlx.Select(&nodes, SelectLeafNodes); err != nil {
//	//	return nil, err
//	//}
//	//return nodes, nil
//	panic("a")
//}
//
//func (mysql *mysqlDBShoppingListRepository) SelectNodes(parentId int64) ([]Node, error) {
//	//var nodes []Node
//	//if err := mysql.db.sqlx.Select(&nodes, SelectNodes, parentId); err != nil {
//	//	return nil, err
//	//}
//	//return nodes, nil
//	panic("a")
//}
//
////func (mysql *mysqlDBShoppingListRepository) SelectRootNode() (*Node, error) {
////	node := Node{}
////	err := mysql.db.sqlx.Get(&node, SelectRootNodeSql)
////	if err != nil {
////		if err == sql.ErrNoRows {
////			return nil, ErrRootNodeNotExist
////		}
////		return nil, err
////	}
////	return &node, nil
////}
//
//func (mysql *mysqlDBShoppingListRepository) Connect() {
//	db := NewMySQLDatabase()
//	db.Connect("shopping_list")
//	mysql.db = &db
//}
//
//func (mysql mysqlDBShoppingListRepository) InsertRootNode(name string, description string) (int64, error) {
//	//category := Category{ParentId:sql.NullInt64{Valid:false}, Name:name, Description: sql.NullString{String:description, Valid:true}, OrderAt: 0}
//	//if _, err := mysql.SelectRootNode(); err != ErrRootNodeNotExist {
//	//	return -1, ErrRootNodeExist
//	//}
//	//rootId, err := mysql.InsertCategory(category)
//	//if err != nil {
//	//	return -1, nil
//	//}
//	//return rootId, nil
//	panic("aa")
//}
//
//func (mysql mysqlDBShoppingListRepository) InsertNodes(parentId int64, names []string) ([]int64, error) {
//	ids := make([]int64, 0)
//	for i, name := range names {
//		if id, err := mysql.InsertNode(parentId, name, int32(i + 1)); err != nil {
//			return []int64{-1}, err
//		} else {
//			ids = append(ids, id)
//		}
//	}
//	return ids, nil
//}
//
//func (mysql mysqlDBShoppingListRepository) DeleteItems() {
//	//mysql.db.sqlx.MustExec(DeleteListItemSql)
//	panic("")
//}
//
//func (mysql mysqlDBShoppingListRepository) DeleteCategories() {
//	mysql.db.sqlx.MustExec(DeleteCategorySql)
//}
//
//func (mysql mysqlDBShoppingListRepository) InsertItem(item Item) (int64, error) {
//	result, err := mysql.db.sqlx.NamedExec(InsertItemSql, map[string]interface{}{"hyperlink": item.Hyperlink,
//		"name": item.Name,
//		"description": item.Description,
//		"piece": item.Piece,
//		"unit": item.Unit})
//	if err != nil {
//		return -1, err
//	}
//	id, err := result.LastInsertId()
//	if err != nil {
//		return -1, err
//	}
//	return id, nil
//}
//
//func (mysql mysqlDBShoppingListRepository) InsertCategory(category Category) (int64, error) {
//	result, err := mysql.db.sqlx.NamedExec(InsertCategorySql, map[string]interface{}{"name": category.Name,
//		"parent_id": category.ParentId,
//		"description": category.Description,
//		"created_at": util.CreatedAt(),
//		"order_at": category.OrderAt})
//	if err != nil {
//		return -1, err
//	}
//	id, err := result.LastInsertId()
//	if err != nil {
//		return -1, err
//	}
//	return id, nil
//}
//
//type Category struct {
//	Name                 string
//	ParentId			 sql.NullInt64
//	Id                   int
//	CreatedAt            int64 `db:"created_at"`
//	OrderAt              int32 `db:"order_at"`
//	Description 		 sql.NullString
//}
//
//type Item struct {
//	Name                 string
//	Hyperlink            string
//	Description          string
//	Piece                float64
//	Id                   int64
//	Unit				 Unit
//}
//
//type Node struct {
//	Name                 string
//	Id					 int64
//	Description			 sql.NullString `db:"description"`
//	CreatedAt            int64 `db:"created_at"`
//	OrderAt              int32 `db:"order_at"`
//}
//
//type NodeLevel struct {
//	Name                 string
//	Id					 int64
//	Description			 sql.NullString `db:"description"`
//	CreatedAt            int64 `db:"created_at"`
//	OrderAt              int32 `db:"order_at"`
//	Level                int
//	ParentId			 sql.NullInt64 `db:"parent_id"`
//}
//
//type SectionNode struct {
//	Name                 string
//	Id					 int64
//	Description			 sql.NullString `db:"description"`
//	OrderAt              int32 `db:"order_at"`
//	ParentId			 int64 `db:"parent_id"`
//	ParentName			 int64 `db:"parent_name"`
//}
//
//type PathNode struct {
//	Id	                 int64
//	Name                 string
//	Path			 	 string
//}
//
//type SectionItem struct{
//	//Name                 string
//	//Hyperlink            string
//	//Description          string
//	//Piece                float64
//	Id                   	int64
//	ItemId			 		int64 `db:"item_id"`
//	OrderAt              	int32 `db:"order_at"`
//	//Unit				 Unit
//
//}