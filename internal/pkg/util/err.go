package util

import "errors"

//Errors
var (
	ErrDataCorruption = errors.New("database: data corruption")
	ErrDataNotFound = errors.New("database: data not found")
)
