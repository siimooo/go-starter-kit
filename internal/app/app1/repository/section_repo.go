package repository

const (
	// INSERT
	InsertSectionSql 		= "INSERT INTO section (item_id, category_id, order_at) VALUE (:item_id, :category_id, :order_at)"

	// SELECT
	SelectSectionItemsPaginationByCategoryId = "SELECT Item.id 'Item.id', Item.hyperlink 'Item.hyperlink', Item.name 'Item.name', Item.description 'Item.description', Item.piece 'Item.piece', Item.unit 'Item.unit', section.* FROM section LEFT JOIN item AS Item ON Item.id = item_id LEFT JOIN category as Category ON Category.id = category_id WHERE category_id = ? AND section.order_at > ? ORDER BY order_at ASC LIMIT ?"
	SelectSectionItemsByCategoryId 	= "SELECT Item.id 'Item.id', Item.hyperlink 'Item.hyperlink', Item.name 'Item.name', Item.description 'Item.description', Item.piece 'Item.piece', Item.unit 'Item.unit', section.* FROM section LEFT JOIN item AS Item ON Item.id = item_id LEFT JOIN category as Category ON Category.id = category_id WHERE category_id = ? ORDER BY order_at"
	SelectSections = "SELECT category_id, item_id, id, order_at FROM section"
	SelectSectionById = "SELECT id, category_id, item_id, order_at FROM section WHERE id = ?"

	// UPDATE

	// DELETE
	DeleteSectionSql 		= "DELETE FROM section"

	// JOIN

)

type SectionRepository interface {
	FindAll() ([]Section2, error)
	FindById(sectionId int64) (Section2, error)
	FindSectionItemsByCategoryId(categoryId int64) ([]SectionItem, error)
	FindSectionItemsPaginationByCategoryId(categoryId int64, pageToken int32, pageSize int32) ([]SectionItem, int32, error)

	Create(categoryId int64, itemId int64, orderAt int) (int64, error)

	Update()

	DeleteAll()
}

func NewMySQLSectionRepository(database MySQLDatabase) *SectionMySQLRepository {
	return &SectionMySQLRepository{db: &database}
}

type SectionMySQLRepository struct {
	db *MySQLDatabase
}

func (mysql SectionMySQLRepository) FindSectionItemsPaginationByCategoryId(categoryId int64, pageToken int32, pageSize int32) ([]SectionItem, int32, error) {
	var sectionItems []SectionItem
	if err := mysql.db.sqlx.Select(&sectionItems, SelectSectionItemsPaginationByCategoryId, categoryId, pageToken, pageSize); err != nil {
		return sectionItems, -1, err
	}
	if sectionItems == nil {
		return sectionItems, 0, nil
	}
	return sectionItems, sectionItems[len(sectionItems) - 1].OrderAt, nil
}

func (mysql SectionMySQLRepository) FindSectionItemsByCategoryId(categoryId int64) ([]SectionItem, error) {
	var sectionItems []SectionItem
	if err := mysql.db.sqlx.Select(&sectionItems, SelectSectionItemsByCategoryId, categoryId); err != nil {
		return sectionItems, err
	}
	return sectionItems, nil
}

func (mysql SectionMySQLRepository) FindAll() ([]Section2, error) {
	var sections []Section2
	if err := mysql.db.sqlx.Select(&sections, SelectSections); err != nil {
		return sections, err
	}
	return sections, nil
}



func (mysql SectionMySQLRepository) FindById(sectionId int64) (Section2, error) {
	section := Section2{}
	if err := mysql.db.sqlx.Get(&section, SelectSectionById, sectionId); err != nil {
		return section, err
	}
	return section, nil
}

func (mysql SectionMySQLRepository) Create(categoryId int64, itemId int64, orderAt int) (int64, error) {
	result, err := mysql.db.sqlx.NamedExec(InsertSectionSql, map[string]interface{}{"category_id": categoryId,
		"item_id":itemId,
		"order_at": orderAt})
	if err != nil {
		return -1, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		return -1, err
	}
	return id, nil
}

func (mysql SectionMySQLRepository) Update() {
	panic("implement me")
}

func (mysql SectionMySQLRepository) DeleteAll() {
	mysql.db.sqlx.MustExec(DeleteSectionSql)
}

type Section2 struct {
	Id                   	int64 `db:"id"`
	ItemId			 		int64 `db:"item_id"`
	CategoryId			 	int64 `db:"category_id"`
	OrderAt              	int32 `db:"order_at"`
}

type SectionItem struct {
	Item2 `db:"Item"`
	Section2
}