package repository

import (
	"bitbucket.com/siimooo/go-starter-kit/internal/pkg/util"
	"log"
	"testing"
)

func TestCategoryMySQLRepository_CreateRoot(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	sectionRepo := NewMySQLSectionRepository(mysql)
	sectionRepo.DeleteAll()
	itemRepo := NewMySQLItemRepository(mysql)
	itemRepo.DeleteAll()
	repo := NewMySQLCategoryRepository(mysql)
	repo.DeleteAll()
	rootId, err := repo.CreateRoot("root", "description", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	cat, err := repo.FindRoot()
	if err != nil {
		t.Fatal(err)
	}
	if cat.Id != rootId {
		t.Fatal("should be same id")
	}
}

func TestCategoryMySQLRepository_CreateRootAlreadyExist(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	sectionRepo := NewMySQLSectionRepository(mysql)
	sectionRepo.DeleteAll()
	itemRepo := NewMySQLItemRepository(mysql)
	itemRepo.DeleteAll()
	repo := NewMySQLCategoryRepository(mysql)
	repo.DeleteAll()
	_, err := repo.CreateRoot("root", "description", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	_, err2 := repo.CreateRoot("root", "description", util.CreatedAt())
	if err2 != ErrRootNodeExist {
		t.Fatal("should be ErrRootNodeExist")
	}
}

func TestCategoryMySQLRepository_FindRootNotFound(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	sectionRepo := NewMySQLSectionRepository(mysql)
	sectionRepo.DeleteAll()
	itemRepo := NewMySQLItemRepository(mysql)
	itemRepo.DeleteAll()
	repo := NewMySQLCategoryRepository(mysql)
	repo.DeleteAll()
	_, err := repo.FindRoot()
	if err != ErrRootNodeNotExist {
		t.Fatal("should be ErrRootNodeNotExist")
	}
}

func TestCategoryMySQLRepository_CreateChild(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	sectionRepo := NewMySQLSectionRepository(mysql)
	sectionRepo.DeleteAll()
	itemRepo := NewMySQLItemRepository(mysql)
	itemRepo.DeleteAll()
	repo := NewMySQLCategoryRepository(mysql)
	repo.DeleteAll()
	rootId, err := repo.CreateRoot("root", "description", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	name := "child_name"
	order := 0
	description := "TODO"
	childId, err := repo.CreateChild(rootId, name, order, description, util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	log.Print(childId)
}

func TestCategoryMySQLRepository_FindAll(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	sectionRepo := NewMySQLSectionRepository(mysql)
	sectionRepo.DeleteAll()
	itemRepo := NewMySQLItemRepository(mysql)
	itemRepo.DeleteAll()
	repo := NewMySQLCategoryRepository(mysql)
	repo.DeleteAll()
	rootId, err := repo.CreateRoot("root", "description", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	if _, err := repo.CreateChild(rootId, "child_name0", 0, "TODO0", util.CreatedAt()); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.CreateChild(rootId, "child_name1", 1, "TODO1", util.CreatedAt()); err != nil {
		t.Fatal(err)
	}
	childId2, err := repo.CreateChild(rootId, "child_name2", 2, "TODO2", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	childId3, err := repo.CreateChild(rootId, "child_name3", 3, "TODO3", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	categories, err := repo.FindAll()
	if err != nil {
		t.Fatal(err)
	}
	if len(categories) != 5 {
		t.Fatal("should be five!")
	}
	child0 := categories[1]
	child1 := categories[2]
	child2 := categories[3]
	child3 := categories[4]
	if child0.Name != "child_name0" {
		t.Fatal("should be name0")
	}
	if child1.Name != "child_name1" {
		t.Fatal("should be name1")
	}
	if child2.Name != "child_name2" {
		t.Fatal("should be name2")
	}
	if child3.Name != "child_name3" {
		t.Fatal("should be name3")
	}
	if child0.OrderAt != 0 {
		t.Fatal("should be 0")
	}
	if child1.OrderAt != 1 {
		t.Fatal("should be 1")
	}
	if child2.OrderAt != 2 {
		t.Fatal("should be 2")
	}
	if child3.OrderAt != 3 {
		t.Fatal("should be 3")
	}
	if child0.Description.String != "TODO0" {
		t.Fatal("should be TODO0")
	}
	if child1.Description.String != "TODO1" {
		t.Fatal("should be TODO1")
	}
	if child2.Description.String != "TODO2" {
		t.Fatal("should be TODO2")
	}
	if child3.Description.String != "TODO3" {
		t.Fatal("should be TODO3")
	}
	if child0.ParentId.Int64 != rootId {
		t.Fatal("should be rootId")
	}
	if child1.ParentId.Int64 != rootId {
		t.Fatal("should be rootId")
	}
	if child2.ParentId.Int64 != rootId {
		t.Fatal("should be rootId")
	}
	if child3.ParentId.Int64 != rootId {
		t.Fatal("should be rootId")
	}
	if child0.Id == 0 {
		t.Fatal("cannot be zero!")
	}
	if child1.Id == 0 {
		t.Fatal("cannot be zero!")
	}
	if child2.Id == 0 {
		t.Fatal("cannot be zero!")
	}
	if child3.Id == 0 {
		t.Fatal("cannot be zero!")
	}
	childchild20Id, err := repo.CreateChild(childId2, "child_child_name2_0", 0, "TODO3", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	childchild30Id, err := repo.CreateChild(childId3, "child_child_name3_0", 0, "TODO3", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	childchild31Id, err := repo.CreateChild(childId3, "child_child_name3_1", 1, "TODO3", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	items, err2 := repo.TreeFindAll()
	if err2 != nil {
		t.Fatal(err)
	}
	root := items.Category2
	if root.ParentId.Valid {
		t.Fatal("should be nil!")
	}
	rootDB, err := repo.FindRoot()
	if err != nil {
		t.Fatal(err)
	}
	compare(root, rootDB, t)
	rootChilds := items.Childs
	if len(rootChilds) != 4 {
		t.Fatal("should be four!")
	}
	rootChild0 := rootChilds[0]
	rootChild1 := rootChilds[1]
	rootChild2 := rootChilds[2]
	rootChild3 := rootChilds[3]
	compare(rootChild0.Category2, child0, t)
	compare(rootChild1.Category2, child1, t)
	compare(rootChild2.Category2, child2, t)
	compare(rootChild3.Category2, child3, t)
	rootChild0Childs := rootChild0.Childs
	rootChild1Childs := rootChild1.Childs
	rootChild2Childs := rootChild2.Childs
	rootChild3Childs := rootChild3.Childs
	if len(rootChild0Childs) != 0 {
		t.Fatal("should be zero!")
	}
	if len(rootChild1Childs) != 0 {
		t.Fatal("should be zero!")
	}
	if len(rootChild2Childs) != 1 {
		t.Fatal("should be one!")
	}
	if len(rootChild3Childs) != 2 {
		t.Fatal("should be two!")
	}
	childchild2_0 := rootChild2Childs[0]
	childchild3_0 := rootChild3Childs[0]
	childchild3_1 := rootChild3Childs[1]


	findchildchild2_0, err := repo.FindById(childchild20Id)
	if err != nil {
		t.Fatal(err)
	}
	findchildchild3_0, err := repo.FindById(childchild30Id)
	if err != nil {
		t.Fatal(err)
	}
	findchildchild3_1, err := repo.FindById(childchild31Id)
	if err != nil {
		t.Fatal(err)
	}
	compare(childchild2_0.Category2, findchildchild2_0, t)
	compare(childchild3_0.Category2, findchildchild3_0, t)
	compare(childchild3_1.Category2, findchildchild3_1, t)

	if len(childchild2_0.Childs) != 0 {
		t.Fatal("should be zero")
	}
	if len(childchild3_0.Childs) != 0 {
		t.Fatal("should be zero")
	}
	if len(childchild3_1.Childs) != 0 {
		t.Fatal("should be zero")
	}
}

func compare(self Category2, to Category2, t *testing.T) {
	if self.ParentId.Valid != to.ParentId.Valid {
		t.Fatal("should be same!")
	}
	if self.ParentId.Int64 != to.ParentId.Int64 {
		t.Fatal("should be same!")
	}
	if self.Description != to.Description {
		t.Fatal("should be same!")
	}
	if self.Description.String != to.Description.String {
		t.Fatal("should be same!")
	}
	if self.Id != to.Id {
		t.Fatal("should be same!")
	}
	if self.CreatedAt != to.CreatedAt {
		t.Fatalf("should be same! self=%v, to=%v", self.Name, to.Name)
	}
	if self.OrderAt != to.OrderAt {
		t.Fatal("should be same!")
	}
	if self.Name != to.Name {
		t.Fatal("should be same!")
	}
}

func TestCategoryMySQLRepository_FindChildrensByParentId(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	sectionRepo := NewMySQLSectionRepository(mysql)
	sectionRepo.DeleteAll()
	itemRepo := NewMySQLItemRepository(mysql)
	itemRepo.DeleteAll()
	repo := NewMySQLCategoryRepository(mysql)
	repo.DeleteAll()
	rootId, err := repo.CreateRoot("root", "description", 0)
	if err != nil {
		t.Fatal(err)
	}
	name0 := "child_name0"
	name1 := "child_name1"
	name2 := "child_name2"
	name3 := "child_name3"
	if _, err := repo.CreateChild(rootId, name0, 0, "TODO0", util.CreatedAt()); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.CreateChild(rootId, name1, 1, "TODO1", util.CreatedAt()); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.CreateChild(rootId, name2, 2, "TODO2", util.CreatedAt()); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.CreateChild(rootId, name3, 3, "TODO3", util.CreatedAt()); err != nil {
		t.Fatal(err)
	}

	childrens, err := repo.FindChildrensByParentId(rootId)
	if err != nil {
		t.Fatal(err)
	}
	if len(childrens) != 4 {
		t.Fatal("should be four!")
	}
	child0 := childrens[0]
	child1 := childrens[1]
	child2 := childrens[2]
	child3 := childrens[3]
	if child0.Name != "child_name0" {
		t.Fatal("should be name0")
	}
	if child1.Name != "child_name1" {
		t.Fatal("should be name1")
	}
	if child2.Name != "child_name2" {
		t.Fatal("should be name2")
	}
	if child3.Name != "child_name3" {
		t.Fatal("should be name3")
	}
	if child0.OrderAt != 0 {
		t.Fatal("should be 0")
	}
	if child1.OrderAt != 1 {
		t.Fatal("should be 1")
	}
	if child2.OrderAt != 2 {
		t.Fatal("should be 2")
	}
	if child3.OrderAt != 3 {
		t.Fatal("should be 3")
	}
	if child0.Description.String != "TODO0" {
		t.Fatal("should be TODO0")
	}
	if child1.Description.String != "TODO1" {
		t.Fatal("should be TODO1")
	}
	if child2.Description.String != "TODO2" {
		t.Fatal("should be TODO2")
	}
	if child3.Description.String != "TODO3" {
		t.Fatal("should be TODO3")
	}
	if child0.ParentId.Int64 != rootId {
		t.Fatal("should be rootId")
	}
	if child1.ParentId.Int64 != rootId {
		t.Fatal("should be rootId")
	}
	if child2.ParentId.Int64 != rootId {
		t.Fatal("should be rootId")
	}
	if child3.ParentId.Int64 != rootId {
		t.Fatal("should be rootId")
	}
	if child0.Id == 0 {
		t.Fatal("cannot be zero!")
	}
	if child1.Id == 0 {
		t.Fatal("cannot be zero!")
	}
	if child2.Id == 0 {
		t.Fatal("cannot be zero!")
	}
	if child3.Id == 0 {
		t.Fatal("cannot be zero!")
	}
}

func TestCategoryMySQLRepository_FindChildrensByLevel(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	sectionRepo := NewMySQLSectionRepository(mysql)
	sectionRepo.DeleteAll()
	itemRepo := NewMySQLItemRepository(mysql)
	repo := NewMySQLCategoryRepository(mysql)
	itemRepo.DeleteAll()
	repo.DeleteAll()
	rootId, err := repo.CreateRoot("root", "description", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	name0 := "child_name0"
	name1 := "child_name1"
	name2 := "child_name2"
	name3 := "child_name3"
	if _, err := repo.CreateChild(rootId, name0, 0, "TODO0", util.CreatedAt()); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.CreateChild(rootId, name1, 1, "TODO1", util.CreatedAt()); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.CreateChild(rootId, name2, 2, "TODO2", util.CreatedAt()); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.CreateChild(rootId, name3, 3, "TODO3", util.CreatedAt()); err != nil {
		t.Fatal(err)
	}

	childrens, err := repo.FindChildrensByLevel(1)
	if err != nil {
		t.Fatal(err)
	}
	if len(childrens) != 4 {
		t.Fatal("should be four!")
	}
	child0 := childrens[0]
	child1 := childrens[1]
	child2 := childrens[2]
	child3 := childrens[3]
	if child0.Name != "child_name0" {
		t.Fatal("should be name0")
	}
	if child1.Name != "child_name1" {
		t.Fatal("should be name1")
	}
	if child2.Name != "child_name2" {
		t.Fatal("should be name2")
	}
	if child3.Name != "child_name3" {
		t.Fatal("should be name3")
	}
	if child0.OrderAt != 0 {
		t.Fatal("should be 0")
	}
	if child1.OrderAt != 1 {
		t.Fatal("should be 1")
	}
	if child2.OrderAt != 2 {
		t.Fatal("should be 2")
	}
	if child3.OrderAt != 3 {
		t.Fatal("should be 3")
	}
	if child0.Description.String != "TODO0" {
		t.Fatal("should be TODO0")
	}
	if child1.Description.String != "TODO1" {
		t.Fatal("should be TODO1")
	}
	if child2.Description.String != "TODO2" {
		t.Fatal("should be TODO2")
	}
	if child3.Description.String != "TODO3" {
		t.Fatal("should be TODO3")
	}
	if child0.ParentId.Int64 != rootId {
		t.Fatal("should be rootId")
	}
	if child1.ParentId.Int64 != rootId {
		t.Fatal("should be rootId")
	}
	if child2.ParentId.Int64 != rootId {
		t.Fatal("should be rootId")
	}
	if child3.ParentId.Int64 != rootId {
		t.Fatal("should be rootId")
	}

	if child0.Level.Int64 != 1 {
		t.Fatal("should be 1")
	}
	if child1.Level.Int64 != 1 {
		t.Fatal("should be 1")
	}
	if child2.Level.Int64 != 1 {
		t.Fatal("should be 1")
	}
	if child3.Level.Int64 != 1 {
		t.Fatal("should be 1")
	}

	if child0.Id == 0 {
		t.Fatal("cannot be zero!")
	}
	if child1.Id == 0 {
		t.Fatal("cannot be zero!")
	}
	if child2.Id == 0 {
		t.Fatal("cannot be zero!")
	}
	if child3.Id == 0 {
		t.Fatal("cannot be zero!")
	}
}