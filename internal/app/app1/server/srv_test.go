package server

import (
	"bitbucket.com/siimooo/go-starter-kit/internal/app/app1/repository"
	"bitbucket.com/siimooo/go-starter-kit/internal/app/app1/rpc"
	eventbus2 "bitbucket.com/siimooo/go-starter-kit/internal/pkg/eventbus"
	"context"
	"github.com/golang/protobuf/ptypes/wrappers"
	"log"
	"testing"
)

func TestShoppingListService_CreateListRoot(t *testing.T) {
	mysql := repository.NewMySQLDatabase()
	mysql.ConnectShoppingList()
	eventbus := eventbus2.NewNATSEventBus()
	eventbus.Connect()
	item := rpc.CreateCategoryParams{Name: "root!", Description:"TODO:", ParentId: nil}
	srv := NewShoppingListService(mysql, eventbus)
	srv.DeleteAll()
	result, err := srv.CreateCategory(context.TODO(), &item)
	if err != nil {
		t.Fatal(err)
	}
	if result.Name != "root!" {
		t.Fatal("should be root! name")
	}
	if result.ParentId != nil {
		t.Fatal("parentId should be nil!")
	}
	if result.Description != "TODO:" {
		t.Fatal("should be TODO:")
	}
	if result.Id == 0 {
		t.Fatal("id should be not zero!")
	}
	if result.CreatedAt == 0 {
		t.Fatal("createdAt should be not zero!")
	}
	root, err := srv.CategoryRepo.FindRoot()
	if err != nil {
		t.Fatal(err)
	}
	if root.Name != "root!" {
		t.Fatal("should be root! name")
	}
	if root.Description.String != "TODO:" {
		t.Fatal("should be TODO:")
	}
	if root.ParentId.Valid != false {
		t.Fatal("should be nil")
	}
	if root.Id != result.Id {
		t.Fatal("should be same id!")
	}
	if root.CreatedAt == 0 {
		t.Fatal("createdAt should be not zero!")
	}
}

func TestShoppingListService_CreateListChild(t *testing.T) {
	mysql := repository.NewMySQLDatabase()
	mysql.ConnectShoppingList()
	eventbus := eventbus2.NewNATSEventBus()
	eventbus.Connect()
	item := rpc.CreateCategoryParams{Name: "root", Description:"TODO:", ParentId: nil}
	srv := NewShoppingListService(mysql, eventbus)
	srv.DeleteAll()
	root, err := srv.CreateCategory(context.TODO(), &item)
	if err != nil {
		t.Fatal(err)
	}
	childItem := rpc.CreateCategoryParams{Name: "child0", Description:"TODO:", ParentId: &wrappers.Int64Value{Value:int64(root.Id)}, OrderAt:int32(0)}
	child, err := srv.CreateCategory(context.TODO(), &childItem)
	if err != nil {
		t.Fatal(err)
	}
	if child.Name != "child0" {
		t.Fatal("should be child0 name")
	}
	if child.Description != "TODO:" {
		t.Fatal("should be TODO:")
	}
	if child.ParentId.Value != root.Id {
		t.Fatal("should be same as rootid")
	}
	if child.CreatedAt == 0 {
		t.Fatal("createdAt should be not zero!")
	}
	result, err := srv.CategoryRepo.FindById(child.Id)
	if err != nil {
		t.Fatal(err)
	}
	if child.Id != result.Id {
		t.Fatal("should be same id!")
	}
	if result.Name != "child0" {
		t.Fatal("should be child0 name")
	}
	if result.Description.String != "TODO:" {
		t.Fatal("should be TODO:")
	}
	if result.Id != child.Id {
		t.Fatal("should be same as rootid")
	}
	if result.CreatedAt == 0 {
		t.Fatal("createdAt should be not zero!")
	}
}

func TestShoppingListService_List(t *testing.T) {
	mysql := repository.NewMySQLDatabase()
	mysql.ConnectShoppingList()
	eventbus := eventbus2.NewNATSEventBus()
	eventbus.Connect()
	srv := NewShoppingListService(mysql, eventbus)
	srv.DeleteAll()

	rootParams := rpc.CreateCategoryParams{Name: "root_srv", OrderAt:0, Description:"root_srv_description"}
	root, err := srv.CreateCategory(context.TODO(), &rootParams)
	if err != nil {
		t.Fatal(err)
	}
	child0Param := rpc.CreateCategoryParams{Name: "child0_srv", OrderAt:0, Description:"child0_srv_description", ParentId: &wrappers.Int64Value{Value:root.Id}}
	child1Param := rpc.CreateCategoryParams{Name: "child1_srv", OrderAt:1, Description:"child1_srv_description", ParentId: &wrappers.Int64Value{Value:root.Id}}
	child2Param := rpc.CreateCategoryParams{Name: "child2_srv", OrderAt:2, Description:"child2_srv_description", ParentId: &wrappers.Int64Value{Value:root.Id}}
	child3Param := rpc.CreateCategoryParams{Name: "child3_srv", OrderAt:3, Description:"child3_srv_description", ParentId: &wrappers.Int64Value{Value:root.Id}}
	child4Param := rpc.CreateCategoryParams{Name: "child4_srv", OrderAt:4, Description:"child4_srv_description", ParentId: &wrappers.Int64Value{Value:root.Id}}
	child5Param := rpc.CreateCategoryParams{Name: "child5_srv", OrderAt:5, Description:"child5_srv_description", ParentId: &wrappers.Int64Value{Value:root.Id}}

	child0, err := srv.CreateCategory(context.TODO(), &child0Param)
	if err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateCategory(context.TODO(), &child1Param); err != nil {
		t.Fatal(err)
	}
	child2, err := srv.CreateCategory(context.TODO(), &child2Param)
	if err != nil {
		t.Fatal(err)
	}
	if _, err :=srv.CreateCategory(context.TODO(), &child3Param); err != nil {
		t.Fatal(err)
	}
	child4, err := srv.CreateCategory(context.TODO(), &child4Param)
	if err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateCategory(context.TODO(), &child5Param); err != nil {
		t.Fatal(err)
	}

	child0Child0Param := rpc.CreateCategoryParams{Name: "child0_child0_srv", OrderAt:0, Description:"child0_child0_srv_description", ParentId: &wrappers.Int64Value{Value:child0.Id}}
	child2Child0Param := rpc.CreateCategoryParams{Name: "child2_child0_srv", OrderAt:0, Description:"child2_child0_srv_description", ParentId: &wrappers.Int64Value{Value:child2.Id}}
	child4Child0Param := rpc.CreateCategoryParams{Name: "child4_child0_srv", OrderAt:0, Description:"child4_child0_srv_description", ParentId: &wrappers.Int64Value{Value:child4.Id}}
	child4Child1Param := rpc.CreateCategoryParams{Name: "child4_child1_srv", OrderAt:1, Description:"child4_child1_srv_description", ParentId: &wrappers.Int64Value{Value:child4.Id}}
	if _, err := srv.CreateCategory(context.TODO(), &child0Child0Param); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateCategory(context.TODO(), &child2Child0Param); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateCategory(context.TODO(), &child4Child0Param); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateCategory(context.TODO(), &child4Child1Param); err != nil {
		t.Fatal(err)
	}
	srvChild0, err := srv.GetCategories(context.TODO(), &rpc.GetCategoriesParams{Id: &wrappers.Int64Value{Value:child0.Id}})
	if err != nil {
		t.Fatal(err)
	}
	srvChild2, err := srv.GetCategories(context.TODO(), &rpc.GetCategoriesParams{Id: &wrappers.Int64Value{Value:child2.Id}})
	if err != nil {
		t.Fatal(err)
	}
	srvChild4, err := srv.GetCategories(context.TODO(), &rpc.GetCategoriesParams{Id: &wrappers.Int64Value{Value:child4.Id}})
	if err != nil {
		t.Fatal(err)
	}
	repoChild0, err := srv.CategoryRepo.TreeFindById(child0.Id)
	if err != nil {
		t.Fatal(err)
	}
	repoChild2, err := srv.CategoryRepo.TreeFindById(child2.Id)
	if err != nil {
		t.Fatal(err)
	}
	repoChild4, err := srv.CategoryRepo.TreeFindById(child4.Id)
	if err != nil {
		t.Fatal(err)
	}
	srvAll, err := srv.GetCategories(context.TODO(), nil)
	if err != nil {
		t.Fatal(err)
	}
	srvAllIdNil, err := srv.GetCategories(context.TODO(), &rpc.GetCategoriesParams{Id:nil})
	if err != nil {
		t.Fatal(err)
	}
	repoAll, err := srv.CategoryRepo.TreeFindAll()
	if err != nil {
		t.Fatal(err)
	}
	compare(repoAll, srvAll, t)
	compare(repoAll, srvAllIdNil, t)

	compare(repoChild0, srvChild0, t)
	compare(repoChild2, srvChild2, t)
	compare(repoChild4, srvChild4, t)
}

func compare(repo repository.Category2Item, srv *rpc.Categories, t *testing.T) {
	if len(repo.Childs) != len(srv.Childs) {
		t.Fatal("child count diff!")
	}
	single(repo.Category2, srv.Category, t)
	for _, repo0 := range repo.Childs {
		for _, srv0 := range srv.Childs {
			if repo0.Category2.Id == srv0.Category.Id {
				single(repo0.Category2, srv0.Category, t)
				if len(repo0.Childs) != len(srv0.Childs) {
					t.Fatal("child count diff!")
				}
			}
			for _, repo1 := range repo0.Childs {
				for _, srv1 := range srv0.Childs {
					if repo1.Category2.Id == srv1.Category.Id {
						if len(repo1.Childs) != len(srv1.Childs) {
							t.Fatal("child count diff!")
						}
						single(repo1.Category2, srv1.Category, t)
					}
				}
			}
		}
	}
}

func single(self repository.Category2, to *rpc.Category, t *testing.T) {
	if self.ParentId.Int64 != to.ParentId.Value {
		t.Fatal("should be same!")
	}
	if self.Description.String != to.Description {
		t.Fatalf("should be same! self=%v, to=%v", self.Name, to.Name)
	}
	if self.Id != to.Id {
		t.Fatal("should be same!")
	}
	if self.CreatedAt != to.CreatedAt {
		t.Fatalf("should be same! self=%v, to=%v", self.Name, to.Name)
	}
	if self.OrderAt != int(to.OrderAt) {
		t.Fatalf("should be same! self=%v, to=%v", self.Name, to.Name)
	}
	if self.Name != to.Name {
		t.Fatal("should be same!")
	}
}


func TestShoppingListService_CreateSection(t *testing.T) {
	mysql := repository.NewMySQLDatabase()
	mysql.ConnectShoppingList()

	eventbus := eventbus2.NewNATSEventBus()
	eventbus.Connect()

	srv := NewShoppingListService(mysql, eventbus)
	srv.DeleteAll()


	item, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"a", Unit:2, Piece:0, Name:"name", Hyperlink:"https://www.google.fi"})
	if err != nil {
		t.Fatal(err)
	}

	root, err := srv.CreateCategory(context.TODO(), &rpc.CreateCategoryParams{Name:"root", Description:"todo"})
	if err != nil {
		t.Fatal(root)
	}

	cat1, err := srv.CreateCategory(context.TODO(), &rpc.CreateCategoryParams{Name: "cat1", Description:"description", ParentId: &wrappers.Int64Value{Value:root.Id}})
	if err != nil {
		t.Fatal(err)
	}
	params := rpc.CreateSectionParams{ItemId:item.Id, CategoryId:cat1.Id, OrderAt: 1}
	if _, err := srv.CreateSection(context.TODO(), &params); err != nil {
		t.Fatal(err)
	}
}

func TestShoppingListService_CreateItem(t *testing.T) {
	mysql := repository.NewMySQLDatabase()
	mysql.ConnectShoppingList()
	eventbus := eventbus2.NewNATSEventBus()
	eventbus.Connect()
	srv := NewShoppingListService(mysql, eventbus)
	srv.DeleteAll()
	item, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Hyperlink:"https://www.google.fi",Name:"ItemFromServie", Piece:2, Description:"Description", Unit: 2})
	if err != nil {
		t.Fatal(err)
	}
	result, err := srv.GetItems(context.TODO(), &rpc.GetItemsParams{PageToken:0, PageSize:10})
	if err != nil {
		t.Fatal(err)
	}
	if len(result.Items) != 1 {
		t.Fatal("should be one!")
	}
	compareItem(*item, *result.Items[0], t)
}

func compareItem(self rpc.Item, to rpc.Item, t *testing.T) {
	if self.Id != to.Id {
		t.Fatal("should be same!")
	}
	if self.Unit != to.Unit {
		t.Fatal("should be same!")
	}
	if self.Piece != to.Piece {
		t.Fatal("should be same!")
	}
	if self.Description != to.Description {
		t.Fatal("should be same!")
	}
	if self.Hyperlink != to.Hyperlink {
		t.Fatal("should be same!")
	}
	if self.Name != to.Name {
		t.Fatal("should be same!")
	}
}


func TestShoppingListService_GetSections(t *testing.T) {
	mysql := repository.NewMySQLDatabase()
	mysql.ConnectShoppingList()
	eventbus := eventbus2.NewNATSEventBus()
	eventbus.Connect()
	srv := NewShoppingListService(mysql, eventbus)
	srv.DeleteAll()

	root, err := srv.CreateCategory(context.TODO(), &rpc.CreateCategoryParams{Name: "root", OrderAt:0, Description:"root_description"})
	if err != nil {
		t.Fatal(err)
	}

	child0, err := srv.CreateCategory(context.TODO(), &rpc.CreateCategoryParams{Name: "child0", OrderAt:0, Description:"child0_description", ParentId: &wrappers.Int64Value{Value:root.Id}})
	if err != nil {
		t.Fatal(err)
	}

	if item, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"0", Unit:1, Piece:0, Name:"0", Hyperlink:"https://www.0.fi"}); err != nil {
		t.Fatal(err)
	} else {
		if _, err := srv.CreateSection(context.TODO(), &rpc.CreateSectionParams{CategoryId:child0.Id, ItemId:item.Id, OrderAt: 1}); err != nil {
			t.Fatal(err)
		}
	}
	if item, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"1", Unit:1, Piece:0, Name:"1", Hyperlink:"https://www.1.fi"}); err != nil {
		t.Fatal(err)
	} else {
		if _, err := srv.CreateSection(context.TODO(), &rpc.CreateSectionParams{CategoryId:child0.Id, ItemId:item.Id, OrderAt: 2}); err != nil {
			t.Fatal(err)
		}
	}
	if item, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"2", Unit:1, Piece:1, Name:"2", Hyperlink:"https://www.2.fi"}); err != nil {
		t.Fatal(err)
	} else {
		if _, err := srv.CreateSection(context.TODO(), &rpc.CreateSectionParams{CategoryId:child0.Id, ItemId:item.Id, OrderAt: 3}); err != nil {
			t.Fatal(err)
		}
	}
	if item, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"3", Unit:2, Piece:3, Name:"3", Hyperlink:"https://www.3.fi"}); err != nil {
		t.Fatal(err)
	} else {
		if _, err := srv.CreateSection(context.TODO(), &rpc.CreateSectionParams{CategoryId:child0.Id, ItemId:item.Id, OrderAt: 4}); err != nil {
			t.Fatal(err)
		}
	}
	response, err := srv.GetSections(context.TODO(), &rpc.GetSectionsParams{CategoryId:child0.Id, PageToken:0, PageSize:20})
	if err != nil {
		t.Fatal(err)
	}
	log.Print(response)
}

func TestShoppingListService_GetItems(t *testing.T) {
	mysql := repository.NewMySQLDatabase()
	mysql.ConnectShoppingList()
	eventbus := eventbus2.NewNATSEventBus()
	eventbus.Connect()
	srv := NewShoppingListService(mysql, eventbus)
	srv.DeleteAll()
	if _, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"0", Unit:1, Piece:0, Name:"0", Hyperlink:"https://www.0.fi"}); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"1", Unit:1, Piece:0, Name:"1", Hyperlink:"https://www.1.fi"}); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"2", Unit:1, Piece:1, Name:"2", Hyperlink:"https://www.2.fi"}); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"3", Unit:2, Piece:3, Name:"3", Hyperlink:"https://www.3.fi"}); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"4", Unit:4, Piece:3, Name:"4", Hyperlink:"https://www.4.fi"}); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"5", Unit:1, Piece:0, Name:"5", Hyperlink:"https://www.5.fi"}); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"6", Unit:1, Piece:0, Name:"6", Hyperlink:"https://www.6.fi"}); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"7", Unit:2, Piece:0, Name:"7", Hyperlink:"https://www.7.fi"}); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"8", Unit:3, Piece:0, Name:"8", Hyperlink:"https://www.8.fi"}); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"9", Unit:1, Piece:0, Name:"9", Hyperlink:"https://www.9.fi"}); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"10", Unit:3, Piece:0, Name:"10", Hyperlink:"https://www.10.fi"}); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"11", Unit:2, Piece:0, Name:"10", Hyperlink:"https://www.11.fi"}); err != nil {
		t.Fatal(err)
	}
	if _, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Description:"12", Unit:1, Piece:0, Name:"10", Hyperlink:"https://www.12.fi"}); err != nil {
		t.Fatal(err)
	}
	response, err := srv.GetItems(context.TODO(), &rpc.GetItemsParams{PageSize:10, PageToken:0})
	if err != nil {
		t.Fatal(err)
	}
	if response.NextPageToken == 0 {
		t.Fatal("should be not zero!")
	}
	if len(response.Items) != 10 {
		t.Fatal("should be 10")
	}
	for _, v := range response.Items {
		if v.Id == 0 {
			t.Fatal("should not be zero!")
		}
		if len(v.Name) == 0 {
			t.Fatal("should not be zero!")
		}
		if len(v.Description) == 0 {
			t.Fatal("should not be zero!")
		}
		if len(v.UnitString) == 0 {
			t.Fatal("should not be zero!")
		}
		if len(v.Hyperlink) == 0 {
			t.Fatal("should not be zero!")
		}
	}
	response2, err2 := srv.GetItems(context.TODO(), &rpc.GetItemsParams{PageSize:10, PageToken:response.NextPageToken})
	if err2 != nil {
		t.Fatal(err)
	}
	if len(response2.Items) != 3 {
		t.Fatal("should be three")
	}
}

func TestShoppingListService_UpdateItem(t *testing.T) {
	mysql := repository.NewMySQLDatabase()
	mysql.ConnectShoppingList()
	eventbus := eventbus2.NewNATSEventBus()
	eventbus.Connect()
	srv := NewShoppingListService(mysql, eventbus)
	srv.DeleteAll()
	item, err := srv.CreateItem(context.TODO(), &rpc.CreateItemParams{Hyperlink:"https://www.google.fiv1",Name:"namev1", Piece:1, Description:"descriptionv1", Unit: 1})
	if err != nil {
		t.Fatal(err)
	}
	updated, err := srv.UpdateItem(context.TODO(), &rpc.UpdateItemParams{Name:"namev2", Id:item.Id, Unit:2, Hyperlink: "https://www.google.fiv2", Description:"descriptionv2", Piece:2})
	if err != nil {
		t.Fatal(err)
	}
	result, err := srv.GetItems(context.TODO(), &rpc.GetItemsParams{PageToken:0, PageSize:10})
	if err != nil {
		t.Fatal(err)
	}
	if len(result.Items) != 1 {
		t.Fatal("should be one!")
	}
	compareItem(*updated, *result.Items[0], t)
}

