package archive

//func TestInsertShoppingList1(t *testing.T) {
//	id := uuid.New()
//	name := "SimoTestaaja"
//	createdAt := util.CreatedAt()
//	orderAt := int32(1)
//	description := "Phasellus interdum nibh ipsum, ut vulputate augue ultrices at"
//	list := List{Name:name, Id:id.String(), OrderAt:orderAt, CreatedAt:createdAt, Description:description}
//	repo := NewMySQLDb()
//	repo.Connect()
//
//	if err := repo.InsertList(list); err != nil {
//		t.Fatal(err)
//	}
//	if err := repo.DeleteListById(id); err != nil {
//		t.Fatal(err)
//	}
//}
//
//func TestSelectShoppingListById1(t *testing.T) {
//	id := uuid.New()
//	name := "TestSelectShoppingListById12"
//	createdAt := util.CreatedAt()
//	orderAt := int32(2)
//	description := "Phasellus interdum nibh ipsum, ut vulputate augue ultrices at"
//	list := List{Name:name, Id:id.String(), OrderAt:orderAt, CreatedAt:createdAt, Description:description}
//	repo := NewMySQLDb()
//	repo.Connect()
//	if err := repo.InsertList(list); err != nil {
//		t.Fatalf("failed to insert! err=%v", err)
//	}
//	item, err := repo.SelectListById(id)
//	if err != nil {
//		t.Fatal(err)
//	}
//	if item == nil {
//		log.Print("item is null!")
//		t.Fatalf("shopping list cannot be nil!")
//	}
//	if item.Name != name {
//		t.Fatal("not same name!")
//	}
//	id2, _ := uuid.Parse(item.Id)
//	if id2 != id {
//		t.Fatal("not same id!")
//	}
//	if item.Name != name {
//		t.Fatal("not same name!")
//	}
//	if item.OrderAt != orderAt {
//		t.Fatal("not same orderAt!")
//	}
//	if item.CreatedAt != createdAt {
//		t.Fatal("not same createdAt!")
//	}
//	if item.Description != description {
//		t.Fatal("not same description!")
//	}
//	if err := repo.DeleteListById(id); err != nil {
//		t.Fatal(err)
//	}
//}
//
//func TestDeleteShoppingListById(t *testing.T) {
//	id := uuid.New()
//	name := "SimoTestaaja"
//	createdAt := util.CreatedAt()
//	orderAt := int32(1)
//	description := "Phasellus interdum nibh ipsum, ut vulputate augue ultrices at"
//	list := List{Name:name, Id:id.String(), OrderAt:orderAt, CreatedAt:createdAt, Description:description}
//
//	repo := NewMySQLDb()
//	repo.Connect()
//	if err := repo.InsertList(list); err != nil {
//		t.Fatal(err)
//	}
//	if err := repo.DeleteListById(id); err != nil {
//		t.Fatal(err)
//	}
//	if err := repo.DeleteListById(id); err == nil {
//		t.Fatal("should not bet nil")
//	}
//}
//
//func TestDeleteAll1(t *testing.T) {
//	repo := NewMySQLDb()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteCategories()
//	repo.DeleteLists()
//	repo.DeleteItems()
//}
//
//func TestMysqlDBShoppingListRepository_InsertShoppingListItem1(t *testing.T) {
//	piece := int32(2)
//	description := "Duis eget dolor libero. Nulla sed purus vel lacus tincidunt gravida."
//	hyperlink := "https://www.google.fi"
//	name := "Vesipumppu"
//	id := uuid.New()
//
//	item := Item{Id: id.String(), Piece:piece,Hyperlink:hyperlink, Description:description, Name:name}
//
//	repo := NewMySQLDb()
//	repo.Connect()
//	if err := repo.InsertItem(item); err != nil {
//		t.Fatal(err)
//	}
//}
//
//
//func TestMysqlDBShoppingListRepository_SelectShoppingListItemById(t *testing.T) {
//	piece := int32(5)
//	description := "Duis eget dolor libero. Nulla sed purus vel lacus tincidunt gravida."
//	hyperlink := "https://www.putin.fi"
//	name := "LOL!"
//	id := uuid.New()
//	item := Item{Id: id.String(), Piece:piece,Hyperlink:hyperlink, Description:description, Name:name}
//	repo := NewMySQLDb()
//	repo.Connect()
//	if err := repo.InsertItem(item); err != nil {
//		t.Fatal(err)
//	}
//	item2, err := repo.SelectItemById(id)
//	if err != nil {
//		t.Fatal(err)
//	}
//	if item2.Name != name {
//		t.Fatal("not same name!")
//	}
//	id2, _ := uuid.Parse(item2.Id)
//	if id2 != id {
//		t.Fatal("not same id!")
//	}
//	if item2.Description != description {
//		t.Fatal("not same description!")
//	}
//	if item2.Piece != piece {
//		t.Fatal("not same piece!")
//	}
//	if item2.Hyperlink != hyperlink {
//		t.Fatal("not same hyperlink!")
//	}
//}
//
//func TestMysqlDBShoppingListRepository_DeleteShoppingListItemById(t *testing.T) {
//	piece := int32(5)
//	description := "Duis eget dolor libero. Nulla sed purus vel lacus tincidunt gravida111e23213."
//	hyperlink := "https://www.putin.fi"
//	name := "LOL!"
//	id := uuid.New()
//	item := Item{Id: id.String(), Piece:piece,Hyperlink:hyperlink, Description:description, Name:name}
//	repo := NewMySQLDb()
//	repo.Connect()
//	if err := repo.InsertItem(item); err != nil {
//		t.Fatal(err)
//	}
//	if err := repo.DeleteItemById(id); err != nil {
//		t.Fatal(err)
//	}
//	if err := repo.DeleteListById(id); err == nil {
//		t.Fatal("should not bet nil")
//	}
//}
//
//func TestMysqlDBShoppingListRepository_SelectShoppingListById_NotFound(t *testing.T) {
//	repo := NewMySQLDb()
//	repo.Connect()
//	id, _ := uuid.Parse("a7334583-f68f-4c72-b249-8fc6c2164878")
//	_, err := repo.SelectListById(id)
//	if err != ErrShoppingListNotFound {
//		t.Fatal("should be ErrShoppingListNotFound error!")
//	}
//}
//
//func TestMysqlDBShoppingListRepository_SelectShoppingListItemById_NotFound(t *testing.T) {
//	repo := NewMySQLDb()
//	repo.Connect()
//	id, _ := uuid.Parse("a7334583-f68f-4c72-b249-8fc6c2164878")
//	_, err := repo.SelectItemById(id)
//	if err != ErrShoppingListItemNotFound {
//		t.Fatal("should be ErrShoppingListItemNotFound error!")
//	}
//}
//
//func TestMysqlDBShoppingListRepository_InsertShoppingListCategoryErr1(t *testing.T) {
//	name := "CategoryName"
//	orderAt := int32(1)
//	listId := uuid.New()
//	id := uuid.New()
//
//	repo := NewMySQLDb()
//	repo.Connect()
//
//	category := Category{Name:name, OrderAt:orderAt, ListId:listId.String(), Id:id.String()}
//	if err := repo.InsertCategory(category); err == nil {
//		t.Fatal("error should be not nil!")
//	}
//}
//
//func TestMysqlDBShoppingListRepository_InsertShoppingListCategory(t *testing.T) {
//	listId := uuid.New()
//	repo := NewMySQLDb()
//	repo.Connect()
//	list := List{Id: listId.String(), Name:"Name", OrderAt:int32(2), Description:"Desc", CreatedAt: util.CreatedAt()}
//	if err := repo.InsertList(list); err != nil {
//		t.Fatal(err)
//	}
//	name := "CategoryName"
//	orderAt := int32(1)
//	id := uuid.New()
//	category := Category{Name:name, OrderAt:orderAt, ListId:listId.String(), Id:id.String()}
//	if err := repo.InsertCategory(category); err != nil {
//		t.Fatal(err)
//	}
//}
//
//func TestMysqlDBShoppingListRepository_InsertShoppingListSection(t *testing.T) {
//	repo := NewMySQLDb()
//	repo.Connect()
//
//	//create list
//	listId := uuid.New()
//	list := List{Id: listId.String(), Name:"Name", OrderAt:int32(2), Description:"Desc", CreatedAt: util.CreatedAt()}
//	if err := repo.InsertList(list); err != nil {
//		t.Fatal(err)
//	}
//
//	//create category
//	categoryId := uuid.New()
//	category := Category{Name:"categoryName", OrderAt:int32(5), ListId:listId.String(), Id:categoryId.String()}
//	if err := repo.InsertCategory(category); err != nil {
//		t.Fatal(err)
//	}
//
//	//create item
//	itemId := uuid.New()
//	item := Item{Id: itemId.String(), Piece:int32(5),Hyperlink:"hyperlink", Description:"description", Name:"itemName"}
//	if err := repo.InsertItem(item); err != nil {
//		t.Fatal(err)
//	}
//
//	//create section
//	sectionId := uuid.New()
//	section := Section{Id:sectionId.String(), CategoryId:categoryId.String(), ItemId:itemId.String(), RowAt:int32(1), SectionAt:int32(2)}
//	if err := repo.InsertSection(section); err != nil {
//		t.Fatal(err)
//	}
//}
//
//func TestMysqlDBShoppingListRepository_Join1(t *testing.T) {
//	repo := NewMySQLDb()
//	repo.Connect()
//
//	//create list
//	listId := uuid.New()
//	list := List{Id: listId.String(), Name:"Root", OrderAt:int32(2), Description:"Desc", CreatedAt: util.CreatedAt()}
//	if err := repo.InsertList(list); err != nil {
//		t.Fatal(err)
//	}
//
//	//create category
//	categoryId := uuid.New()
//	category := Category{Name:"Kalusteet", OrderAt:int32(5), ListId:listId.String(), Id:categoryId.String()}
//	if err := repo.InsertCategory(category); err != nil {
//		t.Fatal(err)
//	}
//	//Finding the leaf nodes
//	/*
//		WITH RECURSIVE category_path (id, name, path) AS
//		(
//		  SELECT BIN_TO_UUID(id,true), name, name as path
//		    FROM category
//		    WHERE BIN_TO_UUID(list_id, true)='fb97bd2c-9638-4750-8946-9f535c341c70'
//		  UNION ALL
//		  SELECT c.id, c.name, CONCAT(cp.path, ' > ', c.name)
//		    FROM category_path AS cp JOIN category AS c
//		      ON cp.id = c.list_id
//		)
//		SELECT * FROM category_path
//		ORDER BY path;
//
//	*/
//}

