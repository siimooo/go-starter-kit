package server

import (
	"bitbucket.com/siimooo/go-starter-kit/internal/app/app1/repository"
	"bitbucket.com/siimooo/go-starter-kit/internal/app/app1/rpc"
	"github.com/twitchtv/twirp"
)

type ShoppingListValidator interface {
	ValidCreateSection(params rpc.CreateSectionParams) error
	ValidCreateItem(params rpc.CreateItemParams) error
	ValidUpdateItem(params rpc.UpdateItemParams) error
	ValidCreateList(params rpc.CreateCategoryParams) error
	ValidGetSections(params rpc.GetSectionsParams) error
}

type Validator struct {}

func (Validator) ValidUpdateItem(params rpc.UpdateItemParams) error {
	if params.Id == 0 {
		return twirp.RequiredArgumentError("id")
	}
	if len(params.Name) == 0 {
		return twirp.RequiredArgumentError("name")
	}
	if len(params.Hyperlink) == 0 {
		return twirp.RequiredArgumentError("hyperlink")
	}
	if len(params.Description) == 0 {
		return twirp.RequiredArgumentError("description")
	}
	if _, err :=repository.Unit(params.Unit).Validate(); err != nil {
		return twirp.InvalidArgumentError("unit", err.Error())
	}
	return nil
}

func (Validator) ValidGetSections(params rpc.GetSectionsParams) error {
	if params.CategoryId == 0 {
		return twirp.RequiredArgumentError("category_id")
	}
	return nil
}


func (Validator) ValidCreateSection(params rpc.CreateSectionParams) error {
	if params.ItemId == 0 {
		return twirp.RequiredArgumentError("item_id")
	}
	if params.CategoryId == 0 {
		return twirp.RequiredArgumentError("category_id")
	}
	if params.OrderAt == 0 {
		return twirp.RequiredArgumentError("order_at")
	}
	return nil
}

func (Validator) ValidCreateItem(params rpc.CreateItemParams) error {
	if len(params.Name) == 0 {
		return twirp.RequiredArgumentError("name")
	}
	if len(params.Hyperlink) == 0 {
		return twirp.RequiredArgumentError("hyperlink")
	}
	if len(params.Description) == 0 {
		return twirp.RequiredArgumentError("description")
	}
	if _, err :=repository.Unit(params.Unit).Validate(); err != nil {
		return twirp.InvalidArgumentError("unit", err.Error())
	}
	return nil
}

func NewShoppingListValidator() ShoppingListValidator {
	return Validator{}
}

func (Validator) ValidCreateList(params rpc.CreateCategoryParams) error {
	if len(params.Name) == 0 {
		return twirp.RequiredArgumentError("name")
	}
	if len(params.Description) == 0 {
		return twirp.RequiredArgumentError("description")
	}
	return nil
}