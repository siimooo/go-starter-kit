package util

import (
	"database/sql"
	"log"
)

func IsValidDelete(result sql.Result, err error) error {
	if err != nil {
		return err
	}
	count, rowsErr := result.RowsAffected()
	if rowsErr != nil {
		return rowsErr
	}
	if count == 0 {
		return ErrDataNotFound
	}
	if count > 1 {
		log.Panic(ErrDataCorruption)
	}
	return nil
}
