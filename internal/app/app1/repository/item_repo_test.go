package repository

import (
	"testing"
)

func TestItemMySQLRepository_CreateFindById(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	sectionRepo := NewMySQLSectionRepository(mysql)
	sectionRepo.DeleteAll()
	catRepo := NewMySQLCategoryRepository(mysql)
	catRepo.DeleteAll()
	repo := NewMySQLItemRepository(mysql)
	repo.DeleteAll()
	item := Item2{Name:"Item0", Description:"Description0", Piece:2, Hyperlink:"https://www.google.fi", Unit:Unit(KILOGRAM)}
	itemId, err := repo.Create(item)
	if err != nil {
		t.Fatal(err)
	}
	selected, err := repo.FindById(itemId)
	if err != nil {
		t.Fatal(err)
	}
	if selected.Name != "Item0" {
		t.Fatal("should be same Item0!")
	}
	if selected.Id != itemId {
		t.Fatal("should be same id!")
	}
	if selected.Hyperlink != "https://www.google.fi" {
		t.Fatal("should be same hyperlink!")
	}
	if selected.Description != "Description0" {
		t.Fatal("should be same Description!")
	}
	if selected.Piece != 2 {
		t.Fatal("should be same Piece!")
	}
	if selected.Unit != KILOGRAM {
		t.Fatal("should be same Unit!")
	}
}

func TestItemMySQLRepository_CreateUnitUnknown(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	sectionRepo := NewMySQLSectionRepository(mysql)
	sectionRepo.DeleteAll()
	catRepo := NewMySQLCategoryRepository(mysql)
	catRepo.DeleteAll()
	repo := NewMySQLItemRepository(mysql)
	repo.DeleteAll()
	item := Item2{Name:"Item0", Description:"Description0", Piece:100, Hyperlink:"https://www.google.fi", Unit:100}
	itemId, err := repo.Create(item)
	if err != nil {
		t.Fatal(err)
	}
	selected, err := repo.FindById(itemId)
	if err != nil {
		t.Fatal(err)
	}
	if selected.Name != "Item0" {
		t.Fatal("should be same Item0!")
	}
	if selected.Id != itemId {
		t.Fatal("should be same id!")
	}
	if selected.Hyperlink != "https://www.google.fi" {
		t.Fatal("should be same hyperlink!")
	}
	if selected.Description != "Description0" {
		t.Fatal("should be same Description!")
	}
	if selected.Piece != 100 {
		t.Fatal("should be same Piece!")
	}
	if selected.Unit != 100 {
		t.Fatal("should be same Unit!")
	}
	if selected.Unit.String() != "unknown" {
		t.Fatal("should be same Unknown")
	}
}

func TestItemMySQLRepository_FindAll(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	sectionRepo := NewMySQLSectionRepository(mysql)
	sectionRepo.DeleteAll()
	catRepo := NewMySQLCategoryRepository(mysql)
	catRepo.DeleteAll()
	repo := NewMySQLItemRepository(mysql)
	repo.DeleteAll()

	_, err := repo.Create(Item2{Name: "Item0", Piece: 0, Unit:METER, Description:"Description0", Hyperlink:"hypelink0"})
	if err != nil {
		t.Fatal(err)
	}
	_, err1 := repo.Create(Item2{Name: "Item1", Piece: 1, Unit:JM, Description:"Description1", Hyperlink:"hypelink1"})
	if err1 != nil {
		t.Fatal(err1)
	}
	_, err2 := repo.Create(Item2{Name: "Item2", Piece: 2, Unit:KILOGRAM, Description:"Description2", Hyperlink:"hypelink2"})
	if err2 != nil {
		t.Fatal(err2)
	}
	_, err3 := repo.Create(Item2{Name: "Item3", Piece: 3, Unit:PIECES, Description:"Description3", Hyperlink:"hypelink3"})
	if err3 != nil {
		t.Fatal(err3)
	}
	items, err := repo.FindAll()
	if err != nil {
		t.Fatal(err)
	}
	if len(items) != 4 {
		t.Fatal("should be four!")
	}
	item2 := items[2]
	if item2.Id == 0 {
		t.Fatal("should not be zero!")
	}
	if len(item2.Description) == 0 {
		t.Fatal("should not be zero!")
	}
	if len(item2.Name) == 0 {
		t.Fatal("should not be zero!")
	}
	if len(item2.Hyperlink) == 0 {
		t.Fatal("should not be zero!")
	}
	if item2.Piece == 0 {
		t.Fatal("should not be zero!")
	}
}

func TestItemMySQLRepository_FindAllPagination(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	sectionRepo := NewMySQLSectionRepository(mysql)
	sectionRepo.DeleteAll()
	catRepo := NewMySQLCategoryRepository(mysql)
	catRepo.DeleteAll()
	repo := NewMySQLItemRepository(mysql)
	repo.DeleteAll()
	if _, err := repo.Create(Item2{Name: "Item0", Piece: 0, Unit:METER, Description:"Description0", Hyperlink:"hypelink0"}); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.Create(Item2{Name: "Item1", Piece: 0, Unit:METER, Description:"Description1", Hyperlink:"hypelink1"}); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.Create(Item2{Name: "Item2", Piece: 0, Unit:METER, Description:"Description2", Hyperlink:"hypelink2"}); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.Create(Item2{Name: "Item3", Piece: 0, Unit:METER, Description:"Description3", Hyperlink:"hypelink3"}); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.Create(Item2{Name: "Item4", Piece: 0, Unit:METER, Description:"Description4", Hyperlink:"hypelink4"}); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.Create(Item2{Name: "Item5", Piece: 0, Unit:METER, Description:"Description5", Hyperlink:"hypelink5"}); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.Create(Item2{Name: "Item6", Piece: 0, Unit:METER, Description:"Description6", Hyperlink:"hypelink6"}); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.Create(Item2{Name: "Item7", Piece: 0, Unit:METER, Description:"Description7", Hyperlink:"hypelink7"}); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.Create(Item2{Name: "Item8", Piece: 0, Unit:METER, Description:"Description8", Hyperlink:"hypelink8"}); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.Create(Item2{Name: "Item9", Piece: 0, Unit:METER, Description:"Description9", Hyperlink:"hypelink9"}); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.Create(Item2{Name: "Item10", Piece: 0, Unit:METER, Description:"Description10", Hyperlink:"hypelink10"}); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.Create(Item2{Name: "Item11", Piece: 0, Unit:METER, Description:"Description11", Hyperlink:"hypelink11"}); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.Create(Item2{Name: "Item12", Piece: 0, Unit:METER, Description:"Description12", Hyperlink:"hypelink12"}); err != nil {
		t.Fatal(err)
	}
	if _, err := repo.Create(Item2{Name: "Item13", Piece: 0, Unit:METER, Description:"Description13", Hyperlink:"hypelink13"}); err != nil {
		t.Fatal(err)
	}
	items, nextPageToken, err := repo.FindAllPagination(0, 10)
	if err != nil {
		t.Fatal(err)
	}
	if len(items) != 10 {
		t.Fatal("should ten!")
	}
	if nextPageToken == 0 {
		t.Fatal("should not be zero!")
	}
	items2, nextPageToken2, err := repo.FindAllPagination(nextPageToken, 10)
	if err != nil {
		t.Fatal(err)
	}
	if len(items2) != 4 {
		t.Fatal("should be four!")
	}
	if nextPageToken2 == 0 {
		t.Fatal("should not be zero!")
	}
}

func TestItemMySQLRepository_FindAllPaginationZero(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	sectionRepo := NewMySQLSectionRepository(mysql)
	sectionRepo.DeleteAll()
	catRepo := NewMySQLCategoryRepository(mysql)
	catRepo.DeleteAll()
	repo := NewMySQLItemRepository(mysql)
	repo.DeleteAll()
	items, nextPageToken, err := repo.FindAllPagination(0, 10)
	if err != nil {
		t.Fatal(err)
	}
	if nextPageToken != 0 {
		t.Fatal("should be zero!")
	}
	if len(items) != 0 {
		t.Fatal("should be zero!")
	}
}

func TestItemMySQLRepository_Update(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	sectionRepo := NewMySQLSectionRepository(mysql)
	sectionRepo.DeleteAll()
	catRepo := NewMySQLCategoryRepository(mysql)
	catRepo.DeleteAll()
	repo := NewMySQLItemRepository(mysql)
	repo.DeleteAll()
	create := Item2{Hyperlink:"https://www.google.fiv1", Description:"Descriptionv1", Unit:PIECES, Piece: 22, Name:"Nimiv1"}
	id, err := repo.Create(create)
	if err != nil {
		t.Fatal(err)
	}
	update := Item2{Name:"Nimiv2", Piece:555, Unit:METER, Description:"descriptionv2", Hyperlink:"https:/www.google.fiv2", Id:id}
	_, err2 := repo.Update(update)
	if err2 != nil {
		t.Fatal(err)
	}
	byId, err := repo.FindById(id)
	if err != nil {
		t.Fatal(err)
	}
	if byId.Name != "Nimiv2" {
		t.Fatal("should be v2!")
	}
	if byId.Hyperlink != "https:/www.google.fiv2" {
		t.Fatal("should be v2!")
	}
	if byId.Description != "descriptionv2" {
		t.Fatal("should be v2!")
	}
	if byId.Piece != 555 {
		t.Fatal("should be 555")
	}
	if byId.Unit != METER {
		t.Fatal("should be meter!")
	}
}