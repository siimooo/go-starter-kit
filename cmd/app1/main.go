package main

import (
	"bitbucket.com/siimooo/go-starter-kit/internal/app/app1/repository"
	"bitbucket.com/siimooo/go-starter-kit/internal/app/app1/rpc"
	"bitbucket.com/siimooo/go-starter-kit/internal/app/app1/server"
	eventbus2 "bitbucket.com/siimooo/go-starter-kit/internal/pkg/eventbus"
	"log"
	"net/http"
)

func main() {
	log.Printf("Starting the ShoppingListServer...")

	mysql := repository.NewMySQLDatabase()
	mysql.ConnectShoppingList()

	eventbus := eventbus2.NewNATSEventBus()
	eventbus.Connect()

	svc := server.NewShoppingListService(mysql, eventbus)
	handler := rpc.NewShoppingListServer(svc, nil)
	if err := http.ListenAndServe(":8080", handler); err != nil {
		log.Panic(err)
	}
}