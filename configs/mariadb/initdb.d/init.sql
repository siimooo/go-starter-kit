CREATE DATABASE IF NOT EXISTS shopping_list CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE shopping_list;

-- #img_src TEXT NULL,
-- ITEM --
CREATE TABLE IF NOT EXISTS item (
    id int(10) unsigned NOT NULL AUTO_INCREMENT,
    hyperlink TEXT NOT NULL,
    name VARCHAR(4000) NOT NULL,
    description TEXT NOT NULL,
    piece DOUBLE(10, 2) DEFAULT 0,
    unit SMALLINT DEFAULT 0,
    PRIMARY KEY (id)
);

CREATE TABLE category (
    id int(10) unsigned NOT NULL AUTO_INCREMENT,
    created_at BIGINT UNSIGNED DEFAULT 0,
    name varchar(4000) NOT NULL,
    description TEXT DEFAULT NULL,
    order_at SMALLINT DEFAULT 0,
    parent_id int(10) unsigned DEFAULT NULL,
    FOREIGN KEY (parent_id) REFERENCES category (id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (id)
);
-- To delete a node and its descendants, just remove the node itself, all the descendants will be deleted automatically by the DELETE CASCADE of the foreign key constraint.
CREATE TABLE section (
    id int(10) unsigned NOT NULL AUTO_INCREMENT,
    item_id int(10) unsigned NOT NULL,
    category_id int(10) unsigned NOT NULL,
    order_at SMALLINT DEFAULT 1,
    FOREIGN KEY (category_id) REFERENCES category (id),
    FOREIGN KEY (item_id) REFERENCES item (id),
    PRIMARY KEY (id)
);
-- #img_src TEXT NULL,
/*LIST
CREATE TABLE IF NOT EXISTS list (
    id BINARY(16) NOT NULL,
    name VARCHAR(4000) NOT NULL,
    order_at SMALLINT DEFAULT 0,
    created_at BIGINT UNSIGNED DEFAULT 0,
    description TEXT NOT NULL,
    PRIMARY KEY (id)
);*/

# CREATE TABLE IF NOT EXISTS category (
#     id BINARY(16) NOT NULL,
#     name VARCHAR(4000) NOT NULL,
#     order_at SMALLINT DEFAULT 0,
#     list_id BINARY(16) NOT NULL,
#     FOREIGN KEY (list_id) REFERENCES list (id),
#     PRIMARY KEY (id)
# );

# CREATE TABLE IF NOT EXISTS section (
#     id BINARY(16) NOT NULL,
#     section_at SMALLINT DEFAULT 0,
#     row_at SMALLINT DEFAULT 0,
#     item_id BINARY(16) NOT NULL,
#     category_id BINARY(16) NOT NULL,
#     FOREIGN KEY (item_id) REFERENCES item (id),
#     FOREIGN KEY (category_id) REFERENCES category (id),
#     PRIMARY KEY (id)
# );