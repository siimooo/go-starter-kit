package eventbus

import "testing"

func TestConnectSuccess(t *testing.T) {
	eventbus := NewNATSEventBus()
	eventbus.Connect()
}

func TestPublish1(t *testing.T) {
	eventbus := NewNATSEventBus()
	event := NewEvent("hello-world", 1, []byte("1"))
	eventbus.Connect()
	eventbus.Publish(event)
}