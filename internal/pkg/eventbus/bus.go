package eventbus

import (
	"bitbucket.com/siimooo/go-starter-kit/internal/pkg/util"
	"fmt"
	nats2 "github.com/nats-io/go-nats"
	"log"
	"strconv"
)

// Default Constants
const (
	NATS_HOST = "NATS_HOST"
	NATS_PORT = "NATS_POST"
)

type EventBus interface {
	Publish(event Event) error
	QueueSubscribe()
	Subscribe()
	Unsubscribe()
	Connect()
	Reconnect()
}

type natsEventBus struct {
	uri string
	nc *nats2.Conn
}

func (nats natsEventBus) Publish(event Event) error {
	if !nats.nc.IsConnected() {
		nats.Reconnect()
		return nats2.ErrConnectionClosed
	}
	if err := nats.nc.Publish(event.Subject(), event.Data()); err != nil {
		return err
	}
	nats.nc.Flush()//TODO: ignore?
	if err := nats.nc.LastError(); err != nil {
		log.Printf("Publisher latest error=[%v] \n", err)
		return err
	}
	fmt.Printf("Published! Subject=[%v] Data=[%v] Version=[%v]\n", event.Subject(), event.Data(), event.Version())
	return nil
}

func (nats natsEventBus) QueueSubscribe() {
	panic("implement me")
}

func (nats natsEventBus) Subscribe() {
	panic("implement me")
}

func (nats natsEventBus) Unsubscribe() {
	panic("implement me")
}

func NewNATSEventBus() EventBus {
	host := util.GetEnv(NATS_HOST, "localhost")
	port, _ := strconv.Atoi(util.GetEnv(NATS_PORT, "4222"))
	uri := util.NatsUri(host, port)
	return &natsEventBus{uri: uri, nc: nil}
}

func (nats natsEventBus) Reconnect() {
	log.Printf("Trying reconnect to EventBus.. uri=[%s]", nats.uri)
	_, err := nats2.Connect(nats.uri)
	if err != nil {
		log.Printf("failed to reconnect! error=[%v]", err)
	} else {
		log.Printf("EventBus reconnected!")
	}
}

func (e *natsEventBus) Connect() {
	log.Printf("Starting connect to EventBus... uri=[%s]", e.uri)
	nc, err := nats2.Connect(e.uri)
	if err != nil {
		log.Panic(err)
	}
	e.nc = nc
	log.Printf("EventBus connected!")
}