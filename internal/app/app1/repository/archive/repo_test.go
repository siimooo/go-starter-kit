package archive
//
//import (
//	"database/sql"
//	"log"
//	"testing"
//)
////https://developer.paypal.com/docs/checkout/integrate/#
//func TestMysqlDBShoppingListRepository_InsertItem2(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//
//	name := "Lorem Ipsum"
//	piece := float64(2)
//	description := "Integer placerat lacinia rutrum."
//	hyperlink := "http://www.mtv.fi"
//	unit := Unit(KILOGRAM)
//	log.Print(unit)
//	item := Item{Name: name, Piece: piece, Description: description, Hyperlink: hyperlink, Unit:unit}
//	if itemId, err := repo.InsertItem(item); err != nil {
//		t.Fatal(err)
//	} else {
//		if selected, err := repo.SelectItemById(itemId); err != nil {
//			t.Fatal(err)
//		} else {
//			if selected.Name != name {
//				t.Fatal("should be same name!")
//			}
//			if selected.Unit != unit {
//				t.Fatal("should be same unit!")
//			}
//			if selected.Id != itemId {
//				t.Fatal("should be same id!")
//			}
//			if selected.Hyperlink != hyperlink {
//				t.Fatal("should be same hyperlink")
//			}
//			if selected.Description != description {
//				t.Fatal("should be same description")
//			}
//			if selected.Piece != piece {
//				t.Fatal("should be same piece")
//			}
//		}
//	}
//}
//
//func TestMysqlDBShoppingListRepository_InsertSectionItem(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//
//	name := "Lorem Ipsum"
//	piece := float64(2)
//	description := "Integer placerat lacinia rutrum."
//	hyperlink := "http://www.google.fi"
//	unit := Unit(KILOGRAM)
//	item := Item{Name: name, Piece: piece, Description: description, Hyperlink: hyperlink, Unit:unit}
//	_, err := repo.InsertItem(item)
//	if err != nil {
//		t.Fatal(err)
//	}
//}
//
//func TestMysqlDBShoppingListRepository_InsertItem(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//
//	name := "Lorem Ipsum"
//	piece := float64(2)
//	description := "Integer placerat lacinia rutrum."
//	hyperlink := "http://www.google.fi"
//	unit := Unit(KILOGRAM)
//	item := Item{Name: name, Piece: piece, Description: description, Hyperlink: hyperlink, Unit:unit}
//	itemId, err := repo.InsertItem(item)
//	if err != nil {
//		t.Fatal(err)
//	}
//
//
//	name2 := "Lorem Ipsum2"
//	piece2 := float64(11)
//	description2 := "Integer placerat lacinia rutrum.2"
//	hyperlink2 := "http://www.mtv.fi"
//	unit2 := Unit(METER)
//	item2 := Item{Name: name2, Piece: piece2, Description: description2, Hyperlink: hyperlink2, Unit:unit2}
//	itemId2, err := repo.InsertItem(item2)
//	if err != nil {
//		t.Fatal(err)
//	}
//
//
//	rootId, err := repo.InsertRootNode("root", "Eu libero a arcu aliquet fermentum")
//	if err != nil {
//		t.Fatal(err)
//	}
//	sectionId, err := repo.InsertSectionNode(rootId, "Keittiö", 1)
//	if err != nil {
//		t.Fatal(err)
//	}
//	if err := repo.InsertSectionItem(sectionId, itemId, 0); err != nil {
//		t.Fatal(err)
//	}
//	if err := repo.InsertSectionItem(sectionId, itemId2, 1); err != nil {
//		t.Fatal(err)
//	}
//	levels, err := repo.SelectNodesByLevel(1)
//	if err != nil {
//		t.Fatal(err)
//	}
//	if len(levels) != 1 {
//		t.Fatal("should be one !")
//	}
//	keittio := levels[0]
//	_, err2 := repo.InsertNewSectionNode(keittio.Id)
//	if err2 != nil {
//		t.Fatal(err)
//	}
//	nodes, err := repo.SelectNodes(keittio.Id)
//	if err != nil {
//		t.Fatal(err)
//	}
//	if len(nodes) != 2 {
//		t.Fatal("should be two!")
//	}
//	sections, err := repo.SelectSectionNodes(keittio.Id)
//	if err != nil {
//		t.Fatal(err)
//	}
//	section0 := sections[0]
//	section1 := sections[1]
//	if section0.Name != "Keittiö-section0" {
//		t.Fatal("should be name= Keittiö-section0 !")
//	}
//	if section1.Name != "Keittiö-section1" {
//		t.Fatal("should be name= Keittiö-section1 !")
//	}
//	if keittio.Description.String != "TODO: Keittiö-write-description.." {
//		t.Fatal("TODO: Keittiö-write-description..")
//	}
//	if keittio.OrderAt != 1 {
//		t.Fatal("should be orderAt 1")
//	}
//	if keittio.Level != 1 {
//		t.Fatal("should be level at 1")
//	}
//	//root2, err := repo.SelectRootNode()
//	//categories, err3 := repo.SelectNodesByLevel(1)
//	//if err3 != nil {
//	//	t.Fatal(err)
//	//}
//	//var items = make([]Item, 0)
//	for _, v := range sections {
//		result, err := repo.SelectSectionItemsByParentId(v.Id)
//		if err != nil {
//			t.Fatal(err)
//		}
//		log.Print("---------")
//		log.Print(result)
//	}
//}
//
//func TestMysqlDBShoppingListRepository_InsertCategory(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//	name := "rootnode!"
//	if _, err := repo.InsertRootNode(name, "description"); err != nil {
//		t.Fatal(err)
//	}
//	if node, err := repo.SelectRootNode(); err != nil {
//		t.Fatal(err)
//	} else {
//		if name != node.Name {
//			t.Fatal("should be same!")
//		}
//		description := sql.NullString{String:"Maecenas eu libero a arcu aliquet fermentum. Morbi sit amet nibh nec dui tempor molestie fringilla quis sem. Sed urna lacus, mollis sed est vitae, finibus rutrum odio. Vestibulum sed cursus metus.", Valid:true}
//		leafOrder := int32(22)
//		leafName := "leafName!"
//		parentId := sql.NullInt64{Int64:int64(node.Id), Valid:true}
//		category := Category{Name:leafName, ParentId: parentId, OrderAt:leafOrder, Description:  description}
//		if _, err := repo.InsertCategory(category); err != nil {
//			t.Fatal(err)
//		}
//		if nodes, err := repo.SelectNodes(node.Id); err != nil {
//			t.Fatal(err)
//		}else {
//			if len(nodes) != 1 {
//				t.Fatal("should be one!")
//			}
//			leaf := nodes[0]
//			if leaf.Name != leafName {
//				t.Fatal("should be same name")
//			}
//			if leaf.Description.String != description.String {
//				t.Fatal("should be same description")
//			}
//		}
//
//	}
//}
//
//func TestMysqlDBShoppingListRepository_SelectRootNode(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//
//	name := "HALLLOOOOO"
//
//	if _, err := repo.InsertRootNode(name, "description"); err != nil {
//		t.Fatal(err)
//	}
//	if node, err := repo.SelectRootNode(); err != nil {
//		t.Fatal(err)
//	} else {
//		if name != node.Name {
//			t.Fatal("not same name!")
//		}
//	}
//}
//
//func TestMysqlDBShoppingListRepository_DeleteCategories(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//}
//
//func TestMysqlDBShoppingListRepository_DeleteItems(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//}
//
//func TestMysqlDBShoppingListRepository_InsertNodes(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//	if _, err := repo.InsertRootNode("rootnode", "description"); err != nil {
//		t.Fatal(err)
//	}
//	if node, err := repo.SelectRootNode(); err != nil {
//		t.Fatal(err)
//	}else{
//		nodes := []string{"Sähkölista","Vesilista","Sekalaiset kalusteet"}
//		if _, err := repo.InsertNodes(node.Id, nodes); err != nil {
//			t.Fatal(err)
//		}
//	}
//}
//
//func TestMysqlDBShoppingListRepository_InsertNodesFailure(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//	if _, err := repo.InsertRootNode("rootnode", "description"); err != nil {
//		t.Fatal(err)
//	}
//	if _, err := repo.SelectRootNode(); err != nil {
//		t.Fatal(err)
//	}else{
//		nodes := []string{"Sähkölista","Vesilista","Sekalaiset kalusteet"}
//		if _, err := repo.InsertNodes(909009099, nodes); err == nil {
//			t.Fatal("err should not be nil")
//		}
//	}
//}
//
//func TestMysqlDBShoppingListRepository_SelectNodes(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//	if _, err := repo.InsertRootNode("Yleiset Asiat", "description"); err != nil {
//		t.Fatal(err)
//	}
//	if node, err := repo.SelectRootNode(); err != nil {
//		t.Fatal(err)
//	}else{
//		names := []string{"Keittiö", "Kylpyhuone", "Makuuhuone", "Varasto"}
//		if _, err := repo.InsertNodes(node.Id, names); err != nil {
//			t.Fatal(err)
//		}
//		if nodes, err := repo.SelectNodes(node.Id); err != nil {
//			t.Fatal(err)
//		}else{
//			if len(nodes) != 4 {
//				t.Fatal("should be 4!")
//			}
//		}
//	}
//}
//
//func TestMysqlDBShoppingListRepository_SelectLeafNodes(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//	if _, err := repo.InsertRootNode("Yleiset Asiat","description"); err != nil {
//		t.Fatal(err)
//	}
//	if node, err := repo.SelectRootNode(); err != nil {
//		t.Fatal(err)
//	}else{
//		names := []string{"Keittiö", "Kylpyhuone", "Makuuhuone", "Varasto"}
//		if _, err := repo.InsertNodes(node.Id, names); err != nil {
//			t.Fatal(err)
//		}
//		if nodes, err := repo.SelectNodes(node.Id); err != nil {
//			t.Fatal(err)
//		}else{
//			if len(nodes) != 4 {
//				t.Fatal("should be 4!")
//			}
//		}
//	}
//}
//
//func TestMysqlDBShoppingListRepository_SelectLeafNodesWithSections(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//	if _, err := repo.InsertRootNode("Yleiset Asiat", "description"); err != nil {
//		t.Fatal(err)
//	}
//	if node, err := repo.SelectRootNode(); err != nil {
//		t.Fatal(err)
//	} else {
//		names := []string{"Keittiö", "Kylpyhuone", "Makuuhuone", "Varasto", "Varasto2"}
//		if _, err := repo.InsertNodes(node.Id, names); err != nil {
//			t.Fatal(err)
//		}
//		if nodes, err := repo.SelectLeafNodes(); err != nil {
//			t.Fatal(err)
//		} else {
//			if len(nodes) != 5 {
//				t.Fatal("should be 5!")
//			}
//		}
//	}
//}
//
//func TestMysqlDBShoppingListRepository_SelectTree(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//	description := "Morbi sit amet nibh nec dui tempor molestie fringilla quis sem."
//	if _, err := repo.InsertRootNode("Yleiset Asiat", description); err != nil {
//		t.Fatal(err)
//	}
//	if node, err := repo.SelectRootNode(); err != nil {
//		t.Fatal(err)
//	} else {
//		if node.Description.String != description {
//			t.Fatal("should be same description")
//		}
//		name0 := "Keittiö"
//		name1 := "Kylpyhuone"
//		name2 := "Makuuhuone"
//		name3 := "Varasto"
//		name4 := "Varasto2"
//		names := []string{name0, name1, name2, name3, name4}
//		if _, err := repo.InsertNodes(node.Id, names); err != nil {
//			t.Fatal(err)
//		}
//		if nodes, err := repo.SelectLeafNodes(); err != nil {
//			t.Fatal(err)
//		} else {
//			if len(nodes) != 5 {
//				t.Fatal("should be 5!")
//			}
//			node0 := nodes[0]
//			node1 := nodes[1]
//			node2 := nodes[2]
//			node3 := nodes[3]
//			node4 := nodes[4]
//			if name0 != node0.Name {
//				t.Fatal("not same name0")
//			}
//			if name1 != node1.Name {
//				t.Fatal("not same name1")
//			}
//			if name2 != node2.Name {
//				t.Fatal("not same name2")
//			}
//			if name3 != node3.Name {
//				t.Fatal("not same name3")
//			}
//			if name4 != node4.Name {
//				t.Fatal("not same name4")
//			}
//		}
//	}
//}
//
//func TestMysqlDBShoppingListRepository_InsertSection(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//	repo.InsertRootNode("root", "Lorem Ipsum!")
//	root, _ := repo.SelectRootNode()
//	repo.InsertNodes(root.Id, []string{"Patentti"})
//	nodes, _ := repo.SelectLeafNodes()
//	if len(nodes) != 1 {
//		t.Fatal("should be one!")
//	}
//	node := nodes[0]
//	if _, err := repo.InsertNewSectionNode(node.Id); err != nil {
//		t.Fatal(err)
//	}
//	if _, err := repo.InsertNewSectionNode(node.Id); err != nil {
//		t.Fatal(err)
//	}
//}
//
//
//func TestMysqlDBShoppingListRepository_InsertSection2(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//	repo.InsertRootNode("root", "Lorem Ipsum!")
//	root, _ := repo.SelectRootNode()
//	repo.InsertNodes(root.Id, []string{"Patentti"})
//	repo.InsertNodes(root.Id, []string{"Autot"})
//	nodes, _ := repo.SelectLeafNodes()
//	if len(nodes) != 2 {
//		t.Fatal("should be two!")
//	}
//	node := nodes[0]
//	node1 := nodes[1]
//	if _, err := repo.InsertNewSectionNode(node.Id); err != nil {
//		t.Fatal(err)
//	}
//	if _, err := repo.InsertNewSectionNode(node.Id); err != nil {
//		t.Fatal(err)
//	}
//	if _, err := repo.InsertNewSectionNode(node1.Id); err != nil {
//		t.Fatal(err)
//	}
//
//	if sections, err := repo.SelectSectionNodes(node.Id); err != nil {
//		t.Fatal(err)
//	} else {
//		if len(sections) != 2 {
//			t.Fatal("should be 2!")
//		}
//		section0 := sections[0]
//		section1 := sections[1]
//
//		if section0.ParentId != node.Id {
//			t.Fatal("wrong parentID!")
//		}
//		if section1.ParentId != node.Id {
//			t.Fatal("wrong parentID!")
//		}
//	}
//}
//
//func TestMysqlDBShoppingListRepository_SelectTree2(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//	repo.InsertRootNode("root", "Lorem Ipsum!")
//	root, _ := repo.SelectRootNode()
//	repo.InsertNodes(root.Id, []string{"Keittiö"})
//	repo.InsertNodes(root.Id, []string{"Makuuhuone"})
//	nodes, _ := repo.SelectLeafNodes()
//	if len(nodes) != 2 {
//		t.Fatal("should be two!")
//	}
//	node0 := nodes[0]
//	node1 := nodes[1]
//	if _, err := repo.InsertNewSectionNode(node0.Id); err != nil {
//		t.Fatal(err)
//	}
//	if _, err := repo.InsertNewSectionNode(node0.Id); err != nil {
//		t.Fatal(err)
//	}
//	if _, err := repo.InsertNewSectionNode(node1.Id); err != nil {
//		t.Fatal(err)
//	}
//
//	if sections, err := repo.SelectSectionNodes(node0.Id); err != nil {
//		t.Fatal(err)
//	} else {
//		if len(sections) != 2 {
//			t.Fatal("should be 2!")
//		}
//		section0 := sections[0]
//		section1 := sections[1]
//
//		if section0.ParentId != node0.Id {
//			t.Fatal("wrong parentID!")
//		}
//		if section1.ParentId != node0.Id {
//			t.Fatal("wrong parentID!")
//		}
//	}
//	if nodes, err := repo.SelectNodesByLevel(2); err != nil {
//		t.Fatal(err)
//	} else {
//		if len(nodes) != 3{
//			t.Fatal(nodes)
//		}
//		section0 := nodes[0]
//		section1 := nodes[1]
//		section2 := nodes[2]
//
//		if section0.ParentId.Int64 != int64(node0.Id) {
//			t.Fatal("wrong parentiD! section0")
//		}
//		if section1.ParentId.Int64 != int64(node0.Id) {
//			t.Fatal("wrong parentiD! section1")
//		}
//		if section2.ParentId.Int64 != int64(node1.Id) {
//			t.Fatal("wrong parentiD! for section2")
//		}
//	}
//}
//
//
//func TestMysqlDBShoppingListRepository_SelectSections(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//	repo.InsertRootNode("root", "Lorem Ipsum!")
//	root, _ := repo.SelectRootNode()
//	repo.InsertNodes(root.Id, []string{"Keittiö"})
//	repo.InsertNodes(root.Id, []string{"Makuuhuone"})
//	nodes, _ := repo.SelectLeafNodes()
//	if len(nodes) != 2 {
//		t.Fatal("should be two!")
//	}
//	node0 := nodes[0]
//	node1 := nodes[1]
//	if _, err := repo.InsertNewSectionNode(node0.Id); err != nil {
//		t.Fatal(err)
//	}
//	if _, err := repo.InsertNewSectionNode(node0.Id); err != nil {
//		t.Fatal(err)
//	}
//	if _, err := repo.InsertNewSectionNode(node1.Id); err != nil {
//		t.Fatal(err)
//	}
//
//	if sections, err := repo.SelectSectionNodes(node0.Id); err != nil {
//		t.Fatal(err)
//	} else {
//		if len(sections) != 2 {
//			t.Fatal("should be 2!")
//		}
//		section0 := sections[0]
//		section1 := sections[1]
//
//		if section0.ParentId != node0.Id {
//			t.Fatal("wrong parentID!")
//		}
//		if section1.ParentId != node0.Id {
//			t.Fatal("wrong parentID!")
//		}
//	}
//}
//
//
//func TestMysqlDBShoppingListRepository_SelectTree3(t *testing.T) {
//	repo := NewMySQLShoppingList()
//	repo.Connect()
//	repo.DeleteSections()
//	repo.DeleteItems()
//	repo.DeleteCategories()
//	repo.InsertRootNode("root", "Lorem Ipsum!")
//	root, _ := repo.SelectRootNode()
//	repo.InsertNodes(root.Id, []string{"Keittiö"})
//	repo.InsertNodes(root.Id, []string{"Makuuhuone"})
//	nodes, _ := repo.SelectLeafNodes()
//	if len(nodes) != 2 {
//		t.Fatal("should be two!")
//	}
//	node0 := nodes[0]
//	node1 := nodes[1]
//	if _, err := repo.InsertNewSectionNode(node0.Id); err != nil {
//		t.Fatal(err)
//	}
//	if _, err := repo.InsertNewSectionNode(node0.Id); err != nil {
//		t.Fatal(err)
//	}
//	if _, err := repo.InsertNewSectionNode(node1.Id); err != nil {
//		t.Fatal(err)
//	}
//
//	if paths, err := repo.SelectTree(); err != nil {
//		t.Fatal(err)
//	}else {
//		if len(paths) != 6 {
//			t.Fatal("should be six!")
//		}
//		rootNode, _ := repo.SelectRootNode()
//		path := paths[0]
//		if path.Id != rootNode.Id {
//			t.Fatal("should be same id!")
//		}
//		if len(path.Path) == 0 {
//			t.Fatal("should not be empty!")
//		}
//		if len(path.Name) == 0 {
//			t.Fatal("should not be empty!")
//		}
//	}
//}
//
///*
//WITH RECURSIVE category_path (id, name, path) AS
//(
//  SELECT id, name, name as path
//    FROM category
//    WHERE parent_id IS NULL
//  UNION ALL
//  SELECT c.id, c.name, CONCAT(cp.path, ' > ', c.name)
//    FROM category_path AS cp JOIN category AS c
//      ON cp.id = c.parent_id
//)
//SELECT * FROM category_path
//ORDER BY path;
//
//
//WITH RECURSIVE category_path (id, name, lvl) AS
//(
//  SELECT id, name, 0 lvl
//    FROM category
//    WHERE parent_id IS NULL
//  UNION ALL
//  SELECT c.id, c.name,cp.lvl + 1
//    FROM category_path AS cp JOIN category AS c
//      ON cp.id = c.parent_id
//)
//SELECT * FROM category_path
//ORDER BY lvl;
//WITH RECURSIVE category_path (id, name, path, description) AS(SELECT id, name, name as path, description FROM category WHERE parent_id IS NULL UNION ALL SELECT c.id, c.name, CONCAT(cp.path, ' > ', c.name), c.description FROM category_path AS cp JOIN category AS c ON cp.id = c.parent_id) SELECT * FROM category_path ORDER BY path
//
//
//WITH RECURSIVE category_path (id, name) AS
//(
//  SELECT id, name
//    FROM category
//    WHERE parent_id = 599
//  UNION ALL
//  SELECT c.id, c.name
//    FROM category_path AS cp JOIN category AS c
//      ON cp.id = c.parent_id
//)
//SELECT * FROM category_path;
//
//WITH RECURSIVE category_path (id, name, lvl) AS (SELECT id, name, 0 lvl FROM category WHERE parent_id IS NULL UNION ALL SELECT c.id, c.name, cp.lvl + 1 FROM category_path AS cp JOIN category AS c ON cp.id = c.parent_id) SELECT * FROM category_path where lvl = 1 ORDER BY lvl;
//
//
//WITH RECURSIVE category_path (id, name, path) AS (SELECT id, name, name as path FROM category WHERE parent_id = 288 UNION ALL SELECT c.id, c.name, CONCAT(cp.path, ' > ', c.name) FROM category_path AS cp JOIN category AS c ON cp.id = c.parent_id) SELECT * FROM category_path ORDER BY path;
//
//*/