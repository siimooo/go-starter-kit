package server

import (
	"bitbucket.com/siimooo/go-starter-kit/internal/app/app1/repository"
	"bitbucket.com/siimooo/go-starter-kit/internal/app/app1/rpc"
	"bitbucket.com/siimooo/go-starter-kit/internal/pkg/eventbus"
	"bitbucket.com/siimooo/go-starter-kit/internal/pkg/util"
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	//"github.com/golang/protobuf/ptypes/wrappers"
	"github.com/twitchtv/twirp"
	"log"
)

type ShoppingListService struct {
	ItemRepo repository.ItemRepository
	CategoryRepo repository.CategoryRepository
	SectionRepo repository.SectionRepository
	Events ShoppingListEvents
	Validator ShoppingListValidator
	Nats eventbus.EventBus
}

//Read more:
// 	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//	-	https://cloud.google.com/apis/design/design_patterns#list_pagination
//	-	https://developers.google.com/protocol-buffers/docs/proto
//	-	http://allyouneedisbackend.com/blog/2017/09/24/the-sql-i-love-part-1-scanning-large-table/
//	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

func (srv ShoppingListService) UpdateItem(ctx context.Context, params *rpc.UpdateItemParams) (*rpc.Item, error) {
	if err := srv.Validator.ValidUpdateItem(*params); err != nil {
		return nil, err
	}
	item, err := srv.ItemRepo.Update(repository.Item2{Id: params.Id, Name:params.Name, Unit:repository.Unit(params.Unit), Description:params.Description, Piece:params.Piece, Hyperlink:params.Hyperlink})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}

	return &rpc.Item{Name: item.Name, Description: item.Description, Hyperlink: item.Hyperlink, Id: item.Id, Piece: item.Piece, Unit: item.Unit.AsInt32(), UnitString: repository.Unit(item.Unit).String()}, nil
}

func (srv ShoppingListService) GetItems(ctx context.Context, params *rpc.GetItemsParams) (*rpc.GetItemsResponse, error) {
	items, nextPageToken, err := srv.ItemRepo.FindAllPagination(params.PageToken, params.PageSize)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	return &rpc.GetItemsResponse{Items: toItems(items), NextPageToken:nextPageToken}, nil
}

func (srv ShoppingListService) GetSections(ctx context.Context, params *rpc.GetSectionsParams) (*rpc.GetSectionResponse, error) {
	if err := srv.Validator.ValidGetSections(*params); err != nil {
		return nil, err
	}
	sections, nextPageToken, err := srv.SectionRepo.FindSectionItemsPaginationByCategoryId(params.CategoryId, params.PageToken, params.PageSize)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	return &rpc.GetSectionResponse{NextPageToken:nextPageToken, Sections: toSectionItems(sections)}, nil
}

func (srv ShoppingListService) CreateSection(ctx context.Context, params *rpc.CreateSectionParams) (*empty.Empty, error) {
	if err := srv.Validator.ValidCreateSection(*params); err != nil {
		return nil, err
	}
	_, err := srv.SectionRepo.Create(params.CategoryId, params.ItemId, int(params.OrderAt))
	if err!= nil {
		return nil, twirp.InternalErrorWith(err)
	}
	return &empty.Empty{}, nil
}

func (srv ShoppingListService) CreateCategory(ctx context.Context, params *rpc.CreateCategoryParams) (*rpc.Category, error) {
	if err := srv.Validator.ValidCreateList(*params); err != nil {
		return nil, err
	}
	createdAt := util.CreatedAt()
	if params.ParentId == nil {
		rootId, err := srv.CategoryRepo.CreateRoot(params.Name, params.Description, createdAt)
		if err != nil {
			return nil, twirp.InternalErrorWith(err)
		}
		if err := srv.Nats.Publish(srv.Events.CreateList(rootId)); err != nil {
			log.Printf("Error to create event for the shopping list id=[%v] error=[%v]", rootId, err)//TODO: monitor?
		}
		return &rpc.Category{Name: params.Name, ParentId: nil, Description: params.Description, Id:rootId, CreatedAt: createdAt}, nil
	}
	childId, err := srv.CategoryRepo.CreateChild(params.ParentId.Value, params.Name, int(params.OrderAt), params.Description, createdAt)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	return &rpc.Category{Name: params.Name, ParentId: params.ParentId, Description: params.Description, Id:childId, OrderAt:params.OrderAt, CreatedAt:createdAt}, nil
}

func (srv ShoppingListService) categories(params *rpc.GetCategoriesParams) (repository.Category2Item, error) {
	if params == nil || params.GetId() == nil {
		if categories, err := srv.CategoryRepo.TreeFindAll(); err != nil {
			return categories, twirp.InternalErrorWith(err)
		} else {
			return categories, nil
		}
	}
	categories, err := srv.CategoryRepo.TreeFindById(params.Id.Value)
	if err != nil {
		return categories, err
	}
	return categories, nil
}

func (srv ShoppingListService) GetCategories(ctx context.Context, params *rpc.GetCategoriesParams) (*rpc.Categories, error) {
	categories, err := srv.categories(params)
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	root := toCategory(categories.Category2)
	response := rpc.Categories{}
	response.Category = &root
	response.Childs = []*rpc.Categories{}
	for _, child0 := range categories.Childs {
		c0 := toCategory(child0.Category2)
		response.Childs = append(response.Childs, &rpc.Categories{Category: &c0, Childs: []*rpc.Categories{}})
	}
	for _, child0 := range categories.Childs {
		for _, child1 := range child0.Childs {
			for _, r0 := range response.Childs {
				if r0.Category.Id == child1.Category2.ParentId.Int64 {
					c1 := toCategory(child1.Category2)
					r0.Childs = append(r0.Childs, &rpc.Categories{Category: &c1, Childs: []*rpc.Categories{}})
				}
			}
		}
	}
	return &response, nil
}

func NewShoppingListService(mysql repository.MySQLDatabase, eventbus eventbus.EventBus) ShoppingListService {
	events := NewShoppingListEvents()
	sectionRepo := repository.NewMySQLSectionRepository(mysql)
	itemRepo := repository.NewMySQLItemRepository(mysql)
	categoryRepo := repository.NewMySQLCategoryRepository(mysql)
	validator := NewShoppingListValidator()
	return ShoppingListService{Nats: eventbus, Events:events, SectionRepo:sectionRepo, ItemRepo:itemRepo, Validator:validator, CategoryRepo: categoryRepo}
}

func (srv ShoppingListService) CreateItem(ctx context.Context, item *rpc.CreateItemParams) (*rpc.Item, error) {
	if err := srv.Validator.ValidCreateItem(*item); err != nil {
		return nil, err
	}
	itemId, err := srv.ItemRepo.Create(repository.Item2{Name:item.Name, Unit:repository.Unit(item.Unit), Description:item.Description, Piece:item.Piece, Hyperlink:item.Hyperlink})
	if err != nil {
		return nil, twirp.InternalErrorWith(err)
	}
	return &rpc.Item{Name: item.Name, Description: item.Description, Hyperlink: item.Hyperlink, Id: itemId, Piece: item.Piece, Unit: item.Unit, UnitString: repository.Unit(item.Unit).String()}, nil
}

func (srv ShoppingListService) DeleteAll() {
	srv.SectionRepo.DeleteAll()
	srv.ItemRepo.DeleteAll()
	srv.CategoryRepo.DeleteAll()
}