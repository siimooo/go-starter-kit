package util

import (
	"fmt"
)

//db_user:password@tcp(localhost:3306)/my_db
//root:root@(localhost:3306)/shopping_list
//<username>:<pw>@tcp(<HOST>:<port>)/<dbname>
func MySqlUri(username string, pw string, host string, port int, database string) string {
	return fmt.Sprintf("%s:%s@tcp(%s:%v)/%s", username, pw, host, port, database)
}

//nats://nats:4222
func NatsUri(host string, port int) string {
	return fmt.Sprintf("nats://%s:%v", host, port)
}