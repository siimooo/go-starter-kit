package repository

import (
	"database/sql"
	"errors"
)
// old shit..
//SelectSectionNodes = "SELECT name, id, description, order_at, parent_id FROM category WHERE parent_id = ? ORDER BY order_at"
//SelectNode         = "SELECT id, name, description, created_at, order_at FROM category WHERE id = ?"
//SelectNodes        = "SELECT id, name, description, created_at, order_at FROM category WHERE parent_id = ?"
//SelectLeafNodes    = "SELECT c1.id, c1.name, c1.description, c1.created_at, c1.order_at FROM category c1 LEFT JOIN category c2 ON c2.parent_id = c1.id WHERE c2.id IS NULL"
//SelectTree 				= "WITH RECURSIVE category_path (id, name, path) AS(SELECT id, name, name as path FROM category WHERE parent_id IS NULL UNION ALL SELECT c.id, c.name, CONCAT(cp.path, ' > ', c.name) FROM category_path AS cp JOIN category AS c ON cp.id = c.parent_id) SELECT * FROM category_path ORDER BY path"
//SelectTreeByLvl 		= "WITH RECURSIVE category_path (id, name, level, description, created_at, order_at, parent_id) AS (SELECT id, name, 0 level, description, created_at, order_at, parent_id FROM category WHERE parent_id IS NULL UNION ALL SELECT c.id, c.name, cp.level + 1, c.description, c.created_at, c.order_at, c.parent_id FROM category_path AS cp JOIN category AS c ON cp.id = c.parent_id) SELECT * FROM category_path where level = ? ORDER BY level"
const (
	// SELECT
	SelectCategoryById = "SELECT id, name, parent_id, description, created_at, order_at FROM category WHERE id = ?"
	SelectCategories = "SELECT id, name, description, created_at, order_at, parent_id FROM category ORDER BY order_at"
	SelectRootSql  = "SELECT id, name, description, created_at, order_at FROM category where parent_id IS NULL"
	SelectChildrensByParentId = "SELECT id, name, parent_id, description, created_at, order_at FROM category WHERE parent_id = ? ORDER BY order_at"
	SelectChildrensByLevel 		= "WITH RECURSIVE category_path (id, name, level, description, created_at, order_at, parent_id) AS (SELECT id, name, 0 level, description, created_at, order_at, parent_id FROM category WHERE parent_id IS NULL UNION ALL SELECT c.id, c.name, cp.level + 1, c.description, c.created_at, c.order_at, c.parent_id FROM category_path AS cp JOIN category AS c ON cp.id = c.parent_id) SELECT * FROM category_path where level = ? ORDER BY level"

	// INSERT
	InsertCategorySql		= "INSERT INTO category (name, parent_id, description, created_at, order_at) VALUES (:name, :parent_id, :description, :created_at, :order_at)"

	// UPDATE

	// DELETE
	DeleteCategorySql 		= "DELETE FROM category"
)

type CategoryRepository interface {
	FindRoot() (Category2, error)
	FindChildrensByParentId(parentId int64) ([]Category2, error)
	FindChildrensByLevel(level uint) ([]Category2, error)
	FindAll() ([]Category2, error)
	TreeFindAll() (Category2Item, error)
	TreeFindById(id int64) (Category2Item, error)
	FindById(id int64) (Category2, error)

	Create(category Category2) (int64, error)
	CreateChild(parentId int64, name string, order int, description string, createdAt int64) (int64, error)
	CreateRoot(name string, description string, createdAt int64) (int64, error)

	Update()

	DeleteAll()
}

//Errors
var (
	ErrNotExist = errors.New("shopping_list: not exist")
	ErrRootNodeNotExist = errors.New("shopping_list: root node not exist")
	ErrRootNodeExist = errors.New("shopping_list: root node already exist")
)

//Read more:
// 	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//	-	http://go-database-sql.org/accessing.html
//	-	https://github.com/jmoiron/sqlx/blob/master/sqlx_test.go
//	-	http://jmoiron.github.io/sqlx/
//  -	http://www.mysqltutorial.org/mysql-adjacency-list-tree/
//	- 	https://blog.tekz.io/mysql-adjacency-list-model-for-hierarchical-data-using-cte/
//	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//

//Image : Your second solution is probably the most correct. You should use the HTTP spec and mimetypes the way they were intended and upload the file via multipart/form-data. As far as handling the relationships, I'd use this process (keeping in mind I know zero about your assumptions or system design):
//https://stackoverflow.com/questions/33279153/rest-api-file-ie-images-processing-best-practices
//POST to /users to create the user entity.
//POST the image to /images, making sure to return a Location header to where the image can be retrieved per the HTTP spec.
//PATCH to /users/carPhoto and assign it the ID of the photo given in the Location header of step 2.


func NewMySQLCategoryRepository(database MySQLDatabase) *CategoryMySQLRepository {
	return &CategoryMySQLRepository{db: &database}
}

type CategoryMySQLRepository struct {
	db *MySQLDatabase
}

func (mysql CategoryMySQLRepository) TreeFindById(id int64) (Category2Item, error) {
	items, err := mysql.TreeFindAll()
	if err != nil {
		return items, err
	}
	if items.Category2.Id == id {
		return items, nil
	}
	for _, child0 := range items.Childs {
		if child0.Category2.Id == id {
			return *child0, nil
		}
	}
	return Category2Item{}, ErrNotExist
}

type Category2Item struct {
	Category2 Category2
	Childs []*Category2Item
}

func (mysql CategoryMySQLRepository) TreeFindAll() (Category2Item, error){
	root, err := mysql.FindRoot()
	if err != nil {
		return Category2Item{}, err
	}
	items := Category2Item{Category2: root, Childs: []*Category2Item{}}
	childerns, err := mysql.FindChildrensByParentId(root.Id)
	if err != nil {
		return items, err
	}
	for _, child := range childerns {
		if child.ParentId.Int64 == items.Category2.Id {
			items.Childs = append(items.Childs, &Category2Item{Category2: child, Childs: []*Category2Item{}})
		}
	}
	for _, itemChild := range items.Childs {
		childerns, err := mysql.FindChildrensByParentId(itemChild.Category2.Id)
		if err != nil {
			return items, err
		}
		for _, child := range childerns {
			if child.ParentId.Int64 == itemChild.Category2.Id {
				itemChild.Childs = append(itemChild.Childs, &Category2Item{Category2: child, Childs: []*Category2Item{}})
			}
		}
	}
	return items, nil
}



/*
function buildTree(array $elements, $parentId = 0) {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = buildTree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }
    return $branch;
}
*/

//func walk(categories []Category2, parentId int64) map[int64]*Category2Node {
//	nodes := map[int64]*Category2Node{}
//	for _, v := range categories {
//		if v.ParentId.Valid == false {
//			nodes[v.Id] = &Category2Node{Id:v.Id, Name:v.Name, Childs: []*Category2Node{}}
//		} else {
//			if v.ParentId.Int64 == parentId {
//				walk(categories, v.Id)
//				parent, ok := nodes[v.ParentId.Int64]
//				if ok {
//					parent.Childs = append(parent.Childs, &Category2Node{Id: v.Id, Name: v.Name, Childs: []*Category2Node{}})
//				} else {
//					for _,v := range nodes {
//						fmt.Printf("key[%v] value[%v]\n", k, nodes[k].Childs)
//						log.Print(k)
						//log.Print(v.Name)
					//}
					//nodes[v.Id] = &Category2Node{Id: v.Id, Name: v.Name, Childs: []*Category2Node{}}
					//nodes[parentId] = &Category2Node{Id:v.Id, Name:v.Name, Childs: []*Category2Node{}}
				//}
			//}
		//}

	//}
	//return nodes
//}
/*

The algorithm is pretty simple:

Take the array of all elements and the id of the current parent (initially 0/nothing/null/whatever).
Loop through all elements.

If the parent_id of an element matches the current parent id you got in 1., the element is a child of the parent. Put it in your list of current children (here: $branch).

Call the function recursively with the id of the element you have just identified in 3., i.e. find all children of that element, and add them as children element.

Return your list of found children.


*/
func (mysql CategoryMySQLRepository) FindChildrensByParentId(parentId int64) ([]Category2, error) {
	var categories []Category2
	if err := mysql.db.sqlx.Select(&categories, SelectChildrensByParentId, parentId); err != nil {
		return categories, err
	}
	return categories, nil
}

func (mysql CategoryMySQLRepository) FindChildrensByLevel(level uint) ([]Category2, error) {
	var categories []Category2
	if err := mysql.db.sqlx.Select(&categories, SelectChildrensByLevel, level); err != nil {
		return categories, err
	}
	return categories, nil
}

func (mysql CategoryMySQLRepository) FindAll() ([]Category2, error) {
	var categories []Category2
	if err := mysql.db.sqlx.Select(&categories, SelectCategories); err != nil {
		return categories, err
	}
	return categories, nil
}

func (mysql CategoryMySQLRepository) FindById(id int64) (Category2, error) {
	category := Category2{}
	if err := mysql.db.sqlx.Get(&category, SelectCategoryById, id); err != nil {
		return category, err
	}
	return category, nil
}

func (mysql CategoryMySQLRepository) CreateRoot(name string, description string, createdAt int64) (int64, error) {
	category := Category2{ParentId:sql.NullInt64{Valid:false}, Name:name, Description: sql.NullString{String:description, Valid:true}, OrderAt: 0, CreatedAt:createdAt}
	if _, err := mysql.FindRoot(); err != ErrRootNodeNotExist {
		return -1, ErrRootNodeExist
	}
	rootId, err := mysql.Create(category)
	if err != nil {
		return -1, nil
	}
	return rootId, nil
}

func (mysql CategoryMySQLRepository) CreateChild(parentId int64, name string, order int, description string, createdAt int64) (int64, error) {
	cat := Category2{ParentId:sql.NullInt64{Int64:parentId, Valid:true}, Name:name, OrderAt:order, Description:sql.NullString{String:description, Valid:true}, CreatedAt:createdAt}
	if id, err := mysql.Create(cat); err != nil {
		return -1, err
	} else {
		return id, nil
	}
}

func (mysql CategoryMySQLRepository) FindRoot() (Category2, error) {
	cat := Category2{}
	err := mysql.db.sqlx.Get(&cat, SelectRootSql)
	if err != nil {
		if err == sql.ErrNoRows {
			return cat, ErrRootNodeNotExist
		}
		return cat, err
	}
	return cat, nil
}

func (mysql CategoryMySQLRepository) Create(category Category2) (int64, error) {
	result, err := mysql.db.sqlx.NamedExec(InsertCategorySql, map[string]interface{}{"name": category.Name,
		"parent_id": category.ParentId,
		"description": category.Description,
		"created_at": category.CreatedAt,
		"order_at": category.OrderAt})
	if err != nil {
		return -1, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		return -1, err
	}
	return id, nil
}

func (mysql CategoryMySQLRepository) Update() {
	panic("implement me")
}

func (mysql CategoryMySQLRepository) DeleteAll() {
	mysql.db.sqlx.MustExec(DeleteCategorySql)
}

type Category2 struct {
	Name                 string
	ParentId			 sql.NullInt64 `db:"parent_id"`
	Id                   int64
	CreatedAt            int64 `db:"created_at"`
	OrderAt              int `db:"order_at"`
	Level              	 sql.NullInt64 `db:"level"`
	Description 		 sql.NullString
}