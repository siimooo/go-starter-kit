package server

import "bitbucket.com/siimooo/go-starter-kit/internal/pkg/eventbus"

type ShoppingListEvents interface {
	CreateList(listId int64) eventbus.Event
}

type Events struct {}

func NewShoppingListEvents() ShoppingListEvents {
	return &Events{}
}

func (Events) CreateList(listId int64) eventbus.Event {
	return eventbus.NewEvent(eventbus.CreateShoppingListEvent, eventbus.ShoppingListVersion, eventbus.Int64ToBinary(listId))
}
