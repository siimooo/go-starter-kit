package eventbus

import (
	"encoding/binary"
	"strconv"
)

// Default Constants
const (
	ShoppingListVersion                 = 1
	CreateShoppingListEvent             = "create-shopping-list-event"
	CreateShoppingListItemEvent         = "create-shopping-list-item-event"
)

func name(subject string, version int) string {
	return subject + "-v" + strconv.Itoa(version)
}

// convert int64 to []byte
func Int64ToBinary(num int64) []byte {
	buf := make([]byte, binary.MaxVarintLen64)
	n := binary.PutVarint(buf, num)
	return buf[:n]
}

// convert []byte to int64
// fmt.Printf("x is: %v, n is: %v\n", x, n)
func BinaryToInt64(bytes []byte) int64 {
	x,_ := binary.Varint(bytes)
	return x
}