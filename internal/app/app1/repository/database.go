package repository

import (
	"bitbucket.com/siimooo/go-starter-kit/internal/pkg/util"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"log"
	"strconv"
)

const (
	MysqlRootPassword = "MYSQL_ROOT_PASSWORD"
	MysqlDatabase     = "MYSQL_DATABASE"
	MysqlUser         = "MYSQL_USER"
	MysqlHost         = "MYSQL_HOST"
	MysqlPort         = "MYSQL_PORT"
)

type MySQLDatabase struct {
	sqlx *sqlx.DB
}

func NewMySQLDatabase() MySQLDatabase {
	mysql := MySQLDatabase{sqlx: nil}
	return mysql
}

func (mysql *MySQLDatabase) Connect(fallbackDB string) {
	username := util.GetEnv(MysqlUser, "root")
	password := util.GetEnv(MysqlRootPassword, "root")
	host := util.GetEnv(MysqlHost, "localhost")
	port, _ := strconv.Atoi(util.GetEnv(MysqlPort, "3306"))
	database := util.GetEnv(MysqlDatabase, fallbackDB)
	uri := util.MySqlUri(username, password, host, port, database)
 	log.Printf("Connecting to MySQL... uri=[%s]", uri)
	db := sqlx.MustConnect("mysql", uri)
	mysql.sqlx = db
	log.Printf("Connected to MySQL!")
}

func (mysql *MySQLDatabase) ConnectShoppingList() {
	username := util.GetEnv(MysqlUser, "root")
	password := util.GetEnv(MysqlRootPassword, "root")
	host := util.GetEnv(MysqlHost, "localhost")
	port, _ := strconv.Atoi(util.GetEnv(MysqlPort, "3306"))
	database := util.GetEnv(MysqlDatabase, "shopping_list")
	uri := util.MySqlUri(username, password, host, port, database)
	log.Printf("Connecting to MySQL... uri=[%s]", uri)
	db := sqlx.MustConnect("mysql", uri)
	mysql.sqlx = db
	log.Printf("Connected to MySQL!")
}

func ( MySQLDatabase) Reconnect() {
	panic("implement me")
}

func ( MySQLDatabase) IsConnect() bool {
	return false
}