package repository

import "errors"

const (
	// INSERT
	InsertItemSql			= "INSERT INTO item (hyperlink, name, description, piece, unit) VALUES (:hyperlink, :name, :description, :piece, :unit)"

	// SELECT
	SelectItems 			= "SELECT id, hyperlink, name, description, piece, unit FROM item"
	SelectItemById 			= "SELECT id, hyperlink, name, description, piece, unit FROM item WHERE id = ?"
	SelectItemsPagination 	= "SELECT id, hyperlink, name, description, piece, unit FROM item WHERE id > ? ORDER BY id ASC LIMIT ?"

	// UPDATE
	UpdateItemById 			= "UPDATE item SET name=:name, description=:description, piece=:piece, unit=:unit, hyperlink=:hyperlink WHERE id = :id"

	// DELETE
	DeleteItemSql 		= "DELETE FROM item"
)

//Errors
var (
	ErrInvalidUnit = errors.New("shopping_list: invalid unit type")
)

type ItemRepository interface {
	FindAll() ([]Item2, error)
	FindById(itemId int64) (Item2, error)
	FindAllPagination(pageToken int64, pageSize int32) ([]Item2, int64, error)
	Create(item Item2) (int64, error)

	Update(item Item2) (Item2, error)

	DeleteAll()
}

func NewMySQLItemRepository(database MySQLDatabase) *ItemMySQLRepository {
	return &ItemMySQLRepository{db: &database}
}

type ItemMySQLRepository struct {
	db *MySQLDatabase
}

func (mysql ItemMySQLRepository) FindAllPagination(pageToken int64, pageSize int32) ([]Item2, int64, error) {
	var items []Item2
	if err := mysql.db.sqlx.Select(&items, SelectItemsPagination, pageToken, pageSize); err != nil {
		return items, -1, err
	}
	if items == nil {
		return items, 0, nil
	}
	return items, items[len(items) - 1].Id, nil
}

func (mysql  ItemMySQLRepository) FindAll() ([]Item2, error) {
	var items []Item2
	if err := mysql.db.sqlx.Select(&items, SelectItems); err != nil {
		return items, err
	}
	return items, nil
}

func (mysql ItemMySQLRepository) FindById(itemId int64) (Item2, error) {
	item := Item2{}
	if err := mysql.db.sqlx.Get(&item, SelectItemById, itemId); err != nil {
		return item, err
	}
	return item, nil
}

func (mysql ItemMySQLRepository) Create(item Item2) (int64, error){
	result, err := mysql.db.sqlx.NamedExec(InsertItemSql, map[string]interface{}{"hyperlink": item.Hyperlink,
		"name": item.Name,
		"description": item.Description,
		"piece": item.Piece,
		"unit": item.Unit})
	if err != nil {
		return -1, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		return -1, err
	}
	return id, nil
}

func (mysql ItemMySQLRepository) Update(item Item2) (Item2, error) {
	_, err := mysql.db.sqlx.NamedExec(UpdateItemById, map[string]interface{}{"hyperlink": item.Hyperlink,
		"name": item.Name,
		"description": item.Description,
		"piece": item.Piece,
		"unit": item.Unit,
		"id": item.Id})
	if err != nil {
		return item, err
	}
	return item, nil
}

func (mysql ItemMySQLRepository) DeleteAll() {
	mysql.db.sqlx.MustExec(DeleteItemSql)
}

type Unit int32

const (
	KILOGRAM Unit = iota + 1
	METER
	JM
	PIECES
	M2
)

func (u Unit) Validate() (bool, error) {
	if u > M2 || u < KILOGRAM {
		return false, ErrInvalidUnit
	}
	return true, nil
}

func (u Unit) AsInt32() int32 {
	return int32(u)
}

func (u Unit) String() string {
	// declare an array of strings
	// ... operator counts how many
	// items in the array (7)
	names := [...]string{
		"kg",
		"m",
		"jm",
		"pcs",
		"m2"}
	// prevent panicking in case of
	// `day` is out of range of Units
	if u < KILOGRAM || u > M2 {
		return "unknown"
	}
	// return the name of a Unit
	// constant from the names array
	// above.
	return names[u - 1]
}

type Item2 struct {
	Name                 string
	Hyperlink            string
	Description          string
	Piece                float64
	Id                   int64
	Unit				 Unit
}