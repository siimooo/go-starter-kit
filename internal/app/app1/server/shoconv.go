package server

import (
	"bitbucket.com/siimooo/go-starter-kit/internal/app/app1/repository"
	"bitbucket.com/siimooo/go-starter-kit/internal/app/app1/rpc"
	"github.com/golang/protobuf/ptypes/wrappers"
)

func toCategory(category repository.Category2) rpc.Category {
	return rpc.Category{Id:category.Id, Name:category.Name, OrderAt:int32(category.OrderAt), CreatedAt:category.CreatedAt,ParentId: &wrappers.Int64Value{Value: category.ParentId.Int64}, Description:category.Description.String}
}

func toItem(item repository.Item2) rpc.Item {
	return rpc.Item{Id:item.Id, Description:item.Description, Name: item.Name, Hyperlink:item.Hyperlink, Piece:item.Piece, Unit: item.Unit.AsInt32(), UnitString:item.Unit.String()}
}

func toItems(items []repository.Item2) []*rpc.Item {
	rpcItems := make([]*rpc.Item, 0)
	for _, v := range items {
		item := toItem(v)
		rpcItems = append(rpcItems, &item)
	}
	return rpcItems
}

func toSectionItems(sectionItems []repository.SectionItem) []*rpc.SectionItem {
	rpcSectionItems := make([]*rpc.SectionItem, 0)
	for _, v := range sectionItems {
		item := toItem(v.Item2)
		section := toSection(v.Section2)
		sectionItem := rpc.SectionItem{Section: &section, Item: &item}
		rpcSectionItems =  append(rpcSectionItems, &sectionItem)
	}
	return rpcSectionItems
}

func toSection(section2 repository.Section2) rpc.Section {
	return rpc.Section{CategoryId:section2.CategoryId, OrderAt:section2.OrderAt, ItemId: section2.ItemId}
}