package eventbus

// Event is the interface that a command must implement.
type Event interface {

	// Returns the actual data which is the payload of the event message.
	Data() []byte

	// Subject returns a string descriptor of the command name
	Subject() string

	// Version returns the version of the event
	Version() int
}

// EventDescriptor is an implementation of the event message interface.
type eventDesc struct {
	subject      string
	data 		 []byte
	version int
}

func (e eventDesc) Data() []byte {
	return e.data
}

func (e eventDesc) Subject() string {
	return name(e.subject, e.version)
}

func (e eventDesc) Version() int {
	return e.version
}

func NewEvent(subject string, version int, data []byte) Event {
	return eventDesc{subject:subject, version:version, data:data}
}