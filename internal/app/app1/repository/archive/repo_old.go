package archive
//
//import (
//	"bitbucket.com/siimooo/go-starter-kit/internal/pkg/util"
//	"database/sql"
//	"errors"
//	_ "github.com/go-sql-driver/mysql"
//	"github.com/google/uuid"
//)
//
//const (
//	//Inserts.
//	InsertShoppingListSql         = "INSERT INTO list (id,name,order_at,created_at,description) VALUES (UUID_TO_BIN(:id, true), :name, :order_at, :created_at, :description)"
//	InsertShoppingListItemSql     = "INSERT INTO item (id, hyperlink, name, description,piece) VALUES (UUID_TO_BIN(:id, true), :hyperlink, :name, :description, :piece)"
//	InsertShoppingCategorySql     = "INSERT INTO category (id, order_at, list_id, name) VALUES (UUID_TO_BIN(:id, true), :order_at, UUID_TO_BIN(:list_id, true), :name)"
//	InsertShoppingSectionSql      = "INSERT INTO section (id, section_at, row_at, item_id, category_id) VALUES (UUID_TO_BIN(:id, true), :section_at, :row_at, UUID_TO_BIN(:item_id, true), UUID_TO_BIN(:category_id, true))"
//
//	//Selects.
//	SelectShoppingListByIdSql     = "SELECT BIN_TO_UUID(id, true) as id, name, created_at, order_at, description FROM list WHERE BIN_TO_UUID(id, true)=?"
//	SelectShoppingListItemByIdSql = "SELECT BIN_TO_UUID(id, true) as id, hyperlink, name, description, piece FROM item WHERE BIN_TO_UUID(id, true)=?"
//
//	//Deletes.
//	DeleteShoppingListItem 	     = "DELETE FROM item"
//	DeleteShoppingListCategory 	 = "DELETE FROM category"
//	DeleteShoppingListSection 	 = "DELETE FROM section"
//	DeleteShoppingListByIdSql    = "DELETE FROM list WHERE id = UUID_TO_BIN(:id, true)"
//	DeleteAllShoppingListSql     = "DELETE FROM list"
//	DeleteShoppingListItemByISql = "DELETE FROM item WHERE id = UUID_TO_BIN(:id, true)"
//	DeleteShoppingListSectionByCategoryIdISql = "DELETE FROM section WHERE category_id = UUID_TO_BIN(:category_id, true)"
//	DeleteShoppingListCategoryByListIdByISql = "DELETE FROM category WHERE list_id = UUID_TO_BIN(:list_id, true)"
//)
//
//func (mysql *mysqlDBShoppingListRepository) Connect() {
//	db := NewMySQLDatabase()
//	db.Connect("shopping_list")
//	mysql.db = &db
//}
//
//type inMemoryShoppingListRepository struct {}
//
//type List struct {
//	Name                 string
//	Id                   string
//	Description 		 string
//	CreatedAt            int64 `db:"created_at"`
//	OrderAt              int32 `db:"order_at"`
//}
//
//type Category struct {
//	Name                 string
//	OrderAt              int32 `db:"order_at"`
//	ListId 				 string `db:"list_at"`
//	Id                   string
//}
//
//type Section struct {
//	Id                  string
//	CategoryId			string `db:"category_id"`
//	ItemId				string `db:"item_id"`
//	SectionAt			int32 `db:"section_at"`
//	RowAt 				int32 `db:"row_at"`
//}
//
//type Item struct {
//	Name                 string
//	Hyperlink            string
//	Description          string
//	Piece                int32
//	Id                   string
//}
//
////Errors
//var (
//	ErrShoppingListNotFound = errors.New("shopping_list: did not found any shopping list from database")
//	ErrShoppingListItemNotFound = errors.New("shopping_list: did not found any shopping list item from database")
//)
//
////Read more:
//// 	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
////	-	http://go-database-sql.org/accessing.html
////	-	https://github.com/jmoiron/sqlx/blob/master/sqlx_test.go
////	-	http://jmoiron.github.io/sqlx/
////  -	http://www.mysqltutorial.org/mysql-adjacency-list-tree/
////	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
//type ShoppingListRepository interface {
//	DeleteItems()
//	DeleteCategories()
//	DeleteSections()
//	DeleteLists()
//
//	InsertList(list List) error
//	InsertItem(item Item) error
//	InsertCategory(category Category) error
//	InsertSection(section Section) error
//
//	SelectItemById(id uuid.UUID) (*Item, error)
//	SelectListById(id uuid.UUID) (*List, error)
//}
//
//type mysqlDBShoppingListRepository struct {
//	db *MySQLDatabase
//}
//
//func (mysql mysqlDBShoppingListRepository) DeleteItems() {
//	mysql.db.sqlx.MustExec(DeleteShoppingListItem)
//}
//
//func (mysql mysqlDBShoppingListRepository) DeleteCategories() {
//	mysql.db.sqlx.MustExec(DeleteShoppingListCategory)
//}
//
//func (mysql mysqlDBShoppingListRepository) DeleteSections() {
//	mysql.db.sqlx.MustExec(DeleteShoppingListSection)
//}
//
//func (mysql mysqlDBShoppingListRepository) InsertCategory(category Category) error {
//	_, err := mysql.db.sqlx.NamedExec(InsertShoppingCategorySql, map[string]interface{}{"id": category.Id,
//																						"order_at": category.OrderAt,
//																						"list_id": category.ListId,
//																						 "name": category.Name})
//	if err != nil {
//		return err
//	}
//	return nil
//}
//
//func (mysql mysqlDBShoppingListRepository) InsertSection(section Section) error {
//	_, err := mysql.db.sqlx.NamedExec(InsertShoppingSectionSql, map[string]interface{}{"id": section.Id,
//																					"row_at": section.RowAt,
//																					"section_at": section.SectionAt,
//																					"item_id": section.ItemId,
//																					"category_id": section.CategoryId})
//	if err != nil {
//		return err
//	}
//	return nil
//}
//
//func (mysql mysqlDBShoppingListRepository) InsertItem(item Item) error {
//	_,err := mysql.db.sqlx.NamedExec(InsertShoppingListItemSql,
//		map[string]interface{}{
//			"id":item.Id,
//			"hyperlink": item.Hyperlink,
//			"name": item.Name,
//			"description": item.Description,
//			"piece": item.Piece})
//	if err != nil {
//		return err
//	}
//	return nil
//}
//
//func (mysql mysqlDBShoppingListRepository) SelectItemById(id uuid.UUID) (*Item, error) {
//	item :=Item{}
//	err := mysql.db.sqlx.Get(&item, SelectShoppingListItemByIdSql, id); if err != nil {
//		if err == sql.ErrNoRows {
//			return nil, ErrShoppingListItemNotFound
//		}
//		return nil, err
//	}
//	return &item, nil
//}
//
//func (mysql mysqlDBShoppingListRepository) DeleteItemById(id uuid.UUID) error {
//	result, err := mysql.db.sqlx.NamedExec(DeleteShoppingListItemByISql, map[string]interface{}{"id": id.String()})
//	return util.IsValidDelete(result, err)
//}
//
//func (mysql mysqlDBShoppingListRepository) DeleteLists() {
//	mysql.db.sqlx.MustExec(DeleteAllShoppingListSql)
//}
//
//func (mysql mysqlDBShoppingListRepository) DeleteListById(id uuid.UUID) error {
//	result, err := mysql.db.sqlx.NamedExec(DeleteShoppingListByIdSql, map[string]interface{}{"id": id.String()})
//	return util.IsValidDelete(result, err)
//}
//
//func (mysql mysqlDBShoppingListRepository) SelectListById(id uuid.UUID) (*List, error) {
//	list := List{}
//	err := mysql.db.sqlx.Get(&list, SelectShoppingListByIdSql, id); if err != nil {
//		if err == sql.ErrNoRows {
//			return nil, ErrShoppingListNotFound
//		}
//		return nil, err
//	}
//	return &list, nil
//}
//
//func (mysql mysqlDBShoppingListRepository) InsertList(list List) error {
//	_, err := mysql.db.sqlx.NamedExec(InsertShoppingListSql,
//		map[string]interface{}{
//			"id": list.Id,
//			"name": list.Name,
//			"order_at": list.OrderAt,
//			"created_at": list.CreatedAt,
//			"description": list.Description})
//	if err != nil {
//		return err
//	}
//	return nil
//}
//
//func NewMySQLDb() *mysqlDBShoppingListRepository {
//	return &mysqlDBShoppingListRepository{db: nil}
//}
/*
---------------------------------
|------ Nested set model -------|
|-------------------------------|
|---------- Section ------------|
|-------------------------------|
|Category, ItemID, Section, Row |
| uuid1	    uuid1    0		 0  |
| uuid1		uuid2	 0	 	 1  |
| uuid2		uuid3	 1		 0  |
|-------------------------------|
List
	Category0:
		Section0:
			Row0:
				Item0:
				Item1:
			Row1:
			Row2:
		Section1:
		Section2:
	Category1:
		Section0:
			Row0:
			Row1:
		Section1:
		Section2:
*/