package repository

import (
	"bitbucket.com/siimooo/go-starter-kit/internal/pkg/util"
	"testing"
)

func TestSectionMySQLRepository_Create(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	repo := NewMySQLSectionRepository(mysql)
	repo.DeleteAll()
	itemRepo := NewMySQLItemRepository(mysql)
	itemRepo.DeleteAll()
	catRepo := NewMySQLCategoryRepository(mysql)
	catRepo.DeleteAll()
	rootId, err := catRepo.CreateRoot("root", "description", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	childId, err := catRepo.CreateChild(rootId, "child0", 0, "TODO", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	itemId, err := itemRepo.Create(Item2{Hyperlink:"hyperlink0", Description:"description0", Unit: KILOGRAM, Piece:2, Name:"Itemname!"})
	if err != nil {
		t.Fatal(err)
	}
	sectionId, err := repo.Create(childId, itemId, 1)
	if err != nil {
		t.Fatal(err)
	}
	section, err := repo.FindById(sectionId)
	if err != nil {
		t.Fatal(err)
	}
	if section.Id != sectionId {
		t.Fatal("should be same id")
	}
	if section.CategoryId != childId {
		t.Fatal("should be same categoryid!")
	}
	if section.ItemId != itemId {
		t.Fatal("should be same itemId!")
	}
	if section.OrderAt != 1 {
		t.Fatal("should be same order at!")
	}
	sections, err := repo.FindAll()
	if err != nil {
		t.Fatal(err)
	}
	if len(sections) != 1 {
		t.Fatal("should be one!")
	}

	if sections[0].Id != sectionId {
		t.Fatal("should be same id")
	}
	if sections[0].CategoryId != childId {
		t.Fatal("should be same categoryid!")
	}
	if sections[0].ItemId != itemId {
		t.Fatal("should be same itemId!")
	}
	if sections[0].OrderAt != 1 {
		t.Fatal("should be same order at!")
	}
}

func TestSectionMySQLRepository_FindSectionItemsByCategoryId(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	repo := NewMySQLSectionRepository(mysql)
	repo.DeleteAll()
	itemRepo := NewMySQLItemRepository(mysql)
	itemRepo.DeleteAll()
	catRepo := NewMySQLCategoryRepository(mysql)
	catRepo.DeleteAll()
	rootId, err := catRepo.CreateRoot("root", "description", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	childId, err := catRepo.CreateChild(rootId, "child0", 0, "TODO", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	id0, err := itemRepo.Create(Item2{Hyperlink:"hyperlink0", Description:"description0", Unit: KILOGRAM, Piece:0, Name:"Name0"})
	id1, err := itemRepo.Create(Item2{Hyperlink:"hyperlink1", Description:"description1", Unit: JM, Piece:1, Name:"Name1"})
	id2, err := itemRepo.Create(Item2{Hyperlink:"hyperlink2", Description:"description2", Unit: METER, Piece:2, Name:"Name2"})
	id3, err := itemRepo.Create(Item2{Hyperlink:"hyperlink3", Description:"description3", Unit: PIECES, Piece:3, Name:"Name3"})
	id4, err := itemRepo.Create(Item2{Hyperlink:"hyperlink4", Description:"description4", Unit: PIECES, Piece:4, Name:"Name4"})
	id5, err := itemRepo.Create(Item2{Hyperlink:"hyperlink5", Description:"description5", Unit: METER, Piece:5, Name:"Name5"})
	id6, err := itemRepo.Create(Item2{Hyperlink:"hyperlink6", Description:"description6", Unit: PIECES, Piece:6, Name:"Name6"})
	id7, err := itemRepo.Create(Item2{Hyperlink:"hyperlink7", Description:"description7", Unit: JM, Piece:7, Name:"Name7"})
	id8, err := itemRepo.Create(Item2{Hyperlink:"hyperlink8", Description:"description8", Unit: PIECES, Piece:8, Name:"Name8"})
	id9, err := itemRepo.Create(Item2{Hyperlink:"hyperlink9", Description:"description9", Unit: PIECES, Piece:9, Name:"Name9"})

	repo.Create(childId, id0, 0)
	repo.Create(childId, id1, 1)
	repo.Create(childId, id2, 2)
	repo.Create(childId, id3, 3)
	repo.Create(childId, id4, 4)
	repo.Create(childId, id5, 5)
	repo.Create(childId, id6, 6)
	repo.Create(childId, id7, 7)
	repo.Create(childId, id8, 8)
	repo.Create(childId, id9, 9)

	sectionItems, err := repo.FindSectionItemsByCategoryId(childId)
	if err != nil {
		t.Fatal(err)
	}
	for _, v := range sectionItems {
		if v.Item2.Id == 0 {
			t.Fatal("cannot be zero!")
		}
		if v.Section2.Id == 0 {
			t.Fatal("cannot be zero!")
		}
	}
}

func TestSectionMySQLRepository_FindSectionItemsPaginationByCategoryId(t *testing.T) {
	mysql := NewMySQLDatabase()
	mysql.ConnectShoppingList()
	repo := NewMySQLSectionRepository(mysql)
	repo.DeleteAll()
	itemRepo := NewMySQLItemRepository(mysql)
	itemRepo.DeleteAll()
	catRepo := NewMySQLCategoryRepository(mysql)
	catRepo.DeleteAll()
	rootId, err := catRepo.CreateRoot("root", "description", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	childId, err := catRepo.CreateChild(rootId, "child0", 0, "TODO", util.CreatedAt())
	if err != nil {
		t.Fatal(err)
	}
	id0, err := itemRepo.Create(Item2{Hyperlink:"hyperlink0", Description:"description0", Unit: KILOGRAM, Piece:1, Name:"Name0"})
	id1, err := itemRepo.Create(Item2{Hyperlink:"hyperlink1", Description:"description1", Unit: JM, Piece:2, Name:"Name1"})
	id2, err := itemRepo.Create(Item2{Hyperlink:"hyperlink2", Description:"description2", Unit: METER, Piece:3, Name:"Name2"})
	id3, err := itemRepo.Create(Item2{Hyperlink:"hyperlink3", Description:"description3", Unit: PIECES, Piece:4, Name:"Name3"})
	id4, err := itemRepo.Create(Item2{Hyperlink:"hyperlink4", Description:"description4", Unit: PIECES, Piece:5, Name:"Name4"})
	id5, err := itemRepo.Create(Item2{Hyperlink:"hyperlink5", Description:"description5", Unit: METER, Piece:6, Name:"Name5"})
	id6, err := itemRepo.Create(Item2{Hyperlink:"hyperlink6", Description:"description6", Unit: PIECES, Piece:7, Name:"Name6"})
	id7, err := itemRepo.Create(Item2{Hyperlink:"hyperlink7", Description:"description7", Unit: JM, Piece:8, Name:"Name7"})
	id8, err := itemRepo.Create(Item2{Hyperlink:"hyperlink8", Description:"description8", Unit: PIECES, Piece:9, Name:"Name8"})
	id9, err := itemRepo.Create(Item2{Hyperlink:"hyperlink9", Description:"description9", Unit: PIECES, Piece:10, Name:"Name9"})

	repo.Create(childId, id0, 1)
	repo.Create(childId, id1, 2)
	repo.Create(childId, id2, 3)
	repo.Create(childId, id3, 4)
	repo.Create(childId, id4, 5)
	repo.Create(childId, id5, 7)
	repo.Create(childId, id6, 8)
	repo.Create(childId, id7, 9)
	repo.Create(childId, id8, 10)
	repo.Create(childId, id9, 11)
	repo.Create(childId, id9, 12)
	repo.Create(childId, id9, 13)

	sectionItems,nextToken, err := repo.FindSectionItemsPaginationByCategoryId(childId, 0, 10)
	if err != nil {
		t.Fatal(err)
	}
	if len(sectionItems) != 10 {
		t.Fatal("should be ten!")
	}
	for _, v := range sectionItems {
		if v.Item2.Id == 0 {
			t.Fatal("cannot be zero!")
		}
		if v.Section2.Id == 0 {
			t.Fatal("cannot be zero!")
		}
	}
	if nextToken == 0 {
		t.Fatal("should be not zero!")
	}
}